<?php

namespace App\Http\Controllers\api;

use App\Products;
use App\ProductFeatures;
use App\Features;
use App\Subcategory;
use App\Otp;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dao = new \stdClass();
        $categories = Products::with('ProductFeatures')
        ->get();
        $temp_products =[];
        foreach($categories as $product){
            $products["code"]=$product->code;
            $products["description"]=$product->description;
            $products["price"]=$product->price;
            $products["warranty 2 year"]=$product->warranty2year;
            $products["warranty 3 year"]=$product->warranty3year;
            $products["delivery"]=$product->delivery;
            $products["pole"]=$product->pole;
            $products["speed"]=$product->speed;
            $products["motor"]['volt']=$product->motor_volt;
            $products["motor"]['desc']=$product->motor_desc;
            $products["motor"]['price']=$product->motor_price;
            $products['accessories']["drp"]['desc']=$product->drp_desc;
            $products['accessories']["drp"]['price']=$product->drp_price;
            $products['accessories']["baseplate"]['desc']=$product->baseplate_desc;
            $products['accessories']["baseplate"]['price']=$product->baseplate_price;
            $products["spares"]["flow"]=$product->productfeatures[0]->flow;
            $products["spares"]["head"]=$product->productfeatures[0]->head;
            $products["spares"]["rotor_code"]=$product->productfeatures[0]->rotor_code;
            $products["spares"]["rotor_price"]=$product->productfeatures[0]->rotor_price;
            $products["spares"]["stator_code"]=$product->productfeatures[0]->stator_code;
            $products["spares"]["stator_price"]=$product->productfeatures[0]->stator_price;
            $products["spares"]["shaft_code"]=$product->productfeatures[0]->shaft_code;
            $products["spares"]["shaft_price"]=$product->productfeatures[0]->shaft_price;
            $products["spares"]["mech_seal_code"]=$product->productfeatures[0]->mech_seal_code;
            $products["spares"]["mech_seal_price"]=$product->productfeatures[0]->mech_seal_price;
            $products["spares"]["uj_m_kit_code"]=$product->productfeatures[0]->uj_m_kit_code;
            $products["spares"]["uj_m_kit_price"]=$product->productfeatures[0]->uj_m_kit_price;
            $temp_products[]=$products;
        }
        
        return response(['code'=>200,'product'=>$temp_products,'message'=>"Fetched successfully"], 200);
   
        
        return response(['code'=>200,'product'=>$categories,'message'=>"Fetched successfully"], 200);
        
    }    
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
        'category_name' => 'required|min:3',
        ]); 

    $category = Category::create(['category_name' => $request->category_name]);
    return response(['code'=>200,'message'=>'Category created'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($code,$pole)
    {
        try {
            
            $dao = new \stdClass();
            $categories = Products::with('ProductFeatures')
            ->where(['products.code'=>$code,'products.pole'=>$pole])
            ->get();
            $temp_products =[];

            foreach($categories as $product){
            //details
            $products["details"]["code"]=$product->code;
            $products["details"]["description"]=$product->description;
            $products["details"]["price"]=$product->price;
            $products["details"]["delivery"]=$product->delivery;
            $products["details"]["pole"]=$product->pole;
            $products["details"]["speed"]=$product->speed;
            $products["details"]["flow"]=$product->productfeatures[0]->flow;
            $products["details"]["head"]=$product->productfeatures[0]->head;
            $products['details']['productQuantity']= 1;
            //motor
            $products["motor"]['volt']=$product->motor_volt;
            $products["motor"]['desc']=$product->motor_desc;
            $products["motor"]['price']=$product->motor_price;
            //warranty
            $products["warranty"][0]["duration"]="1 Year";
            $products["warranty"][0]["price"]=0;
            $products["warranty"][0]["desc"]="Standard Warranty";
            $products["warranty"][1]["duration"]="2 Years";
            $products["warranty"][1]["price"]=$product->warranty2year;
            $products["warranty"][1]["desc"]="Price: ".$product->warranty2year;
            $products["warranty"][2]["duration"]="3 Years";
            $products["warranty"][2]["price"]=$product->warranty3year;
            $products["warranty"][2]["desc"]="Price: ".$product->warranty3year;
            //accessoories
            $products['accessories'][0]['imgSrc']="assets/images/icons/accessories2.png";
            $products['accessories'][0]['productName']="Baseplate";
            $products['accessories'][0]['productDesc']=$product->baseplate_desc;
            $products['accessories'][0]['accessoryQuantity']=1;
            $products['accessories'][0]['productPrice']=$product->baseplate_price;
            $products['accessories'][1]['imgSrc']="assets/images/icons/accessories2.png";
            $products['accessories'][1]['productName']="DRP";
            $products['accessories'][1]['productDesc']=$product->drp_desc;
            $products['accessories'][1]['accessoryQuantity']=1;
            $products['accessories'][1]['productPrice']=$product->drp_price;
            //Spares
            $products["spares"][0]["imgSrc"]="assets/images/icons/accessories2.png";
            $products["spares"][0]["productName"]="Rotor";
            $products["spares"][0]["productDesc"]=$product->productfeatures[0]->rotor_code;
            $products["spares"][0]["spareQuantity"]=0;
            $products["spares"][0]["productPrice"]=$product->productfeatures[0]->rotor_price;

            $products["spares"][1]["imgSrc"]="assets/images/icons/accessories2.png";
            $products["spares"][1]["productName"]="Stator";
            $products["spares"][1]["productDesc"]=$product->productfeatures[0]->stator_code;
            $products["spares"][1]["spareQuantity"]=0;
            $products["spares"][1]["productPrice"]=$product->productfeatures[0]->stator_price;

            $products["spares"][2]["imgSrc"]="assets/images/icons/accessories2.png";
            $products["spares"][2]["productName"]="Shaft";
            $products["spares"][2]["productDesc"]=$product->productfeatures[0]->shaft_code;
            $products["spares"][2]["spareQuantity"]=0;
            $products["spares"][2]["productPrice"]=$product->productfeatures[0]->shaft_price;

            $products["spares"][3]["imgSrc"]="assets/images/icons/accessories2.png";
            $products["spares"][3]["productName"]="Mech Seal";
            $products["spares"][3]["productDesc"]=$product->productfeatures[0]->mech_seal_code;
            $products["spares"][3]["spareQuantity"]=0;
            $products["spares"][3]["productPrice"]=$product->productfeatures[0]->mech_seal_price;


            $products["spares"][4]["imgSrc"]="assets/images/icons/accessories2.png";
            $products["spares"][4]["productName"]="UJ M Kit";
            $products["spares"][4]["productDesc"]=$product->productfeatures[0]->uj_m_kit_code;
            $products["spares"][4]["spareQuantity"]=0;
            $products["spares"][4]["productPrice"]=$product->productfeatures[0]->uj_m_kit_price;

            $temp_products[]=$products;
            }
            
            return response(['code'=>200,'product'=>$temp_products,'message'=>"Fetched successfully"], 200);
        } catch (GlobalException $e) {
            return response()->json($e->__toJSON());
        }
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
       

        
    }
}

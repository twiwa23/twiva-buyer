window.addEventListener('DOMContentLoaded', function (event) {
    if (!user_id && !accessToken) {
        goto(UI_URL.buyerLogin);
    }
    country();
    phoneCodes();
    getAllAddress();
    


    const params = new URLSearchParams(window.location.search)
    var address_id = params.get('aid')
    if(address_id) {
        editAddress(address_id);
    } else {
        getSavedAddress();
    }
});


function getAllAddress() {
    __ajax_http("buyer/shipping/address-listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_all_address);
}

function __success_all_address(response) {
    console.log(response);
    $(".new-shipping-address").hide();

    let addressHtml = '';
    response.data.forEach(element => {
        addressHtml +=    '<div class="box-container col-12 existing-address">'
        addressHtml +=        '<div class="select-address"><input type="radio" id="address_id" name="address_id" value="'+ element.id +'" onclick="check()">'
        addressHtml +=        '<h3>'+ element.first_name +' ' + element.last_name +'</h3> </div>'
        addressHtml +=        '<p>'+ element.apartment +', ' + element.locality +', '+ element.city.name +', '+ element.state.name +', '+ element.country.name +', '+ element.postal_code +'</p>'
        addressHtml +=        '<p>Contact No. '+ element.phone_number +'</p>'
        addressHtml +=    '</div>'
    });
    addressHtml +=    '<div class="box-container col-12 add-new-address">'
        addressHtml +=        '<input type="radio" name="address_id" onclick="check()" value="other"> Add Other Address'
        addressHtml +=    '</div>'
    $('.shipping-address').append(addressHtml);

    
}

function check() {
    let value = $('input:radio[name=address_id]:checked').val();
    if(value == "other") {
        $(".new-shipping-address").show();
        // $(".existing-address").hide();
    } else {
        $(".new-shipping-address").hide();
        // $(".existing-address").show();
    }
}


function getSavedAddress() {
    __ajax_http("buyer/shipping/address-listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_saved_address);
}

function __success_saved_address(response) {
    console.log(response);
    let message = window.localStorage.getItem("message");
    if(message) {
        $(".alert-success").show().html(message);
        setTimeout(function() {
            $(".alert-success").hide();
            window.localStorage.removeItem("message");
        }, 4000);
    }
    var addressHtml = '';
    response.data.forEach(element => {
        let postal_code = element.postal_code ? element.postal_code : ''
        addressHtml +=    '<div class="box-container col-12">'
        addressHtml +=        '<h3>'+ element.first_name +' ' + element.last_name +'</h3>'
        addressHtml +=        '<p>'+ element.apartment +', ' + element.locality +', '+ element.city.name +', '+ element.state.name +', '+ element.country.name +', '+ postal_code +'</p>'
        addressHtml +=        '<p>Contact No. '+ element.phone_number +'</p>'
        addressHtml +=        '<ul>'
        addressHtml +=            '<li class="edit"><a onclick="editAddressPage('+ element.id +')">Edit</a></li>'
        addressHtml +=            '<li class="delete"><a onclick="deleteAddress('+ element.id +')">Delete</a></li>'
        addressHtml +=        '</ul>'
        addressHtml +=    '</div>'
    });
    $('.address-box').append(addressHtml);
    
}

function editAddressPage(address_id) {
    location.href = 'buyer-edit-address.php?aid='+address_id;
}

function editAddress(address_id) {
    __ajax_http("buyer/shipping/edit-address/"+address_id, "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_edit_address);
}

function __success_edit_address(response) {
    console.log(response);
    
    if(response.status == true) {
        console.log(response.data.country);
        state(response.data.country)
        city(response.data.state)

        let phone = response.data.phone_number.split('-');
            $("#first_name").val(response.data.first_name);
            $("#last_name").val(response.data.last_name);
            $("#apartment").val(response.data.apartment);
            $("#locality").val(response.data.locality);
            $("#phone_number").val(phone[1]);
            $("#postal_code").val(response.data.postal_code);
            $("#phone-codes").val(phone[0]);

        setInterval(function() {
            $("#country").find('option[value="'+response.data.country+'"]').attr('selected', true);
            $("#state").find('option[value="'+response.data.state+'"]').attr('selected', true);
            $("#city").find('option[value="'+response.data.city+'"]').attr('selected', true);

        }, 1000);
    } else {
        alert("Please select the correct Address");
    }
}

function updateAddressFunc() {
    const params = new URLSearchParams(window.location.search)
    var address_id = params.get('aid')
    let phone_number = $("#phone-codes").val()+'-'+ $("#phone_number").val();
    
    var data = {
        "first_name": $("#first_name").val(),
        "last_name": $("#last_name").val(),
        "apartment": $("#apartment").val(),
        "locality": $("#locality").val(),
        "country": $("#country").val(),
        "state": $("#state").val(),
        "city": $("#city").val(),
        "postal_code": $("#postal_code").val(),
        "phone_number": phone_number
    }
    updateAddress(data, address_id);
}

function updateAddress(data, address_id) {
    __ajax_http("buyer/shipping/update-address/"+address_id, data, headers(), AJAX_CONF.apiType.PUT, "GET Sponsee", __success_update_address);
}

function __success_update_address(response) {
    console.log(response);
    if(response.status == true) {
        // $(".alert-success").show().html("Address Updated Successfully");
        window.localStorage.setItem("message", "Address Updated Successfully");
        location.href = 'buyer-manage-address.php';
        
    }
    if(response.status == false) {
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}


function deleteAddress(address_id) {
    let check = confirm('Are you sure you want to delete this item');
    if(check === false) return;
    __ajax_http("buyer/shipping/delete-address/"+address_id, "", headers(), AJAX_CONF.apiType.DELETE, "GET Sponsee", __success_delete_address);
}


function __success_delete_address(response) {
    if(response.status == true) {
        $(".alert-success").show().html("Address Deleted Successfully");
        setTimeout(function() {
            $(".alert-success").hide();
        }, 4000);
        location.reload();
    }
}


function addAddress() {
    let address_id = $('input:radio[name=address_id]:checked').val();
    console.log(address_id);
    if(address_id !== 'other') {
        window.localStorage.setItem("address_id", address_id);
        goto(UI_URL.buyerPlaceOrder);
        return;
    }
    let phone_number = $("#phone-codes").val()+'-'+ $("#phone_number").val();

    var data = {
        "first_name": $("#first_name").val(),
        "last_name": $("#last_name").val(),
        "apartment": $("#apartment").val(),
        "locality": $("#locality").val(),
        "city": $("#city").val(),
        "country": $("#country").val(),
        "state": $("#state").val(),
        "postal_code": $("#postal_code").val(),
        "phone_number": phone_number
    }

// console.log(data);
    addShippingAddress(data);
}

function addShippingAddress(data) {
    __ajax_http("buyer/shipping/add-address", data, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __success_address_store);
}

function __success_address_store(response) {
    console.log(response);
    if(response.status === true) {
        window.localStorage.setItem("address_id", response.address_id);
        // console.log(response);
        goto(UI_URL.buyerPlaceOrder);
    }
    if(response.status == false) {
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}


function addNewAddress() {
    let phone_number = $("#phone-codes").val()+'-'+ $("#phone_number").val();

    var data = {
        "first_name": $("#first_name").val(),
        "last_name": $("#last_name").val(),
        "apartment": $("#apartment").val(),
        "locality": $("#locality").val(),
        "city": $("#city").val(),
        "country": $("#country").val(),
        "state": $("#state").val(),
        "postal_code": $("#postal_code").val() ?? '',
        "phone_number": phone_number
    }

    addNewShippingAddress(data);
}

function addNewShippingAddress(data) {
    __ajax_http("buyer/shipping/add-address", data, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __success_new_address_store);
}

function __success_new_address_store(response) {
    // console.log(response);
    if(response.status === true) {
        goto(UI_URL.buyerAddress);
    }
    if(response.status == false) {
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}

function country() {
    __ajax_http("country/list", "", { 'Accept': 'application/json' }, AJAX_CONF.apiType.GET,"", __success_country);
}

function __success_country(response) {
    console.log(response);
    if(response.status === 200) {
        response.countries.forEach(element => {
            $('#country').append(`
                <option value="${element.id}">${element.name}</option>`
            ) 
        });
    } else {
        $('#country').append(`
                <option value="">Country Not Found</option>`
        )
    }
}


function state(country_id){

    __ajax_http("state/select?country_id="+country_id, "", { 'Accept': 'application/json' }, AJAX_CONF.apiType.GET,"", __success_state);
       
}

function city(state_id){

    __ajaxregister_http("city/select?state_id="+state_id, "", { 'Accept': 'application/json' }, AJAX_CONF.apiType.GET,"POST LOGIN", __success_city);
       
}


function state1() {
    let country = document.getElementById('country').value;
    var country_id= country;
    state(country_id);
}

function __success_state(response) {
    $('#state').html('Searching States...');
    if(response.states) {
        $(response.states).each(function(index,key){
            $('#state').append(`
                <option value="${key.id}">${key.name}</option>`      
            )   
        })
    } else {
        $('#state').append(`
            <option value="">State Not Found</option>`      
        ) 
    }
    
}

//city fetch

function city1(){
    var state = document.getElementById('state').value;
    var state_id= state;
    city(state_id);
}


function __success_city(response)
{
    $('#city').html('Searching Cities...');
    if(response.cities) {
        $(response.cities).each(function(index,key){
            $('#city').append(`
                <option value="${key.id}">${key.name}</option>` 
            )
        })
    } else {
        $('#city').append(`
            <option value="">City Not Found</option>` 
        )
    }
    
}


// Cart.js

var parsedCartDB;
var totalPrice = 0;
var i = 0;
var cartId;

window.addEventListener('DOMContentLoaded', function (event) {
    updateCartIcon();

    if (user_id && accessToken) {
        return getUserCart();
    }

    if (!parsedCart || parsedCart.length === 0) {
        return showEmptyCartPage();
    }
    
    parsedCart.forEach(async element =>  {
        var res = await fetch(API_URL + "buyer/"+ element['inf_id'] +"/"+ element['pid']+"/details")
                        .then(r => r.json());

        __buyer_cart_details(res, element.id);
    });
    
});



function getUserCart() {
    __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __cartPage);
}

function getProductDetail(inf_id, pid, cb = __buyer_cart_details) {
    __ajax_http("buyer/"+ inf_id +"/"+ pid+"/details", "", { 'Accept': 'application/json' }, AJAX_CONF.apiType.GET, "GET Sponsee", cb);
}

function __cartPage(response) {
    if(response.data.length === 0) {
        return showEmptyCartPage();
    }

    parsedCartDB = response.data;
    response.data.forEach(async element => {
        var res = await fetch(API_URL + "buyer/"+ btoa(btoa(element.influencer_id)) +"/"+ btoa(btoa(element.product_id))+"/details")
                        .then(r => r.json());

        __buyer_cart_details(res, element.id);
        // getProductDetail(btoa(btoa(element.influencer_id)), btoa(btoa(element.product_id)))
    });
}

function __buyer_cart_details(response, cart_id){
    
    var html = '';
    let size = '';
    let color = '';
    let size_id;
    let color_name;
    let color_id;
    var product_detail= response.data.product_details;
    
    var cartData = user_id && accessToken ? parsedCartDB : parsedCart;
    

    for (let i of cartData) {
        if(i["quantity"] < 1) {
            user_id && accessToken ? deleteCartItemFromDB(i["id"]) : deleteCartItem(i["id"]);
            return;
        }
        if(response.data.id != i["product_id"]) {
            continue;
        }

        if(cart_id != i["id"]) {
            continue;
        }
        product_detail.size_details.forEach(element => {
            if(element.id == i["size"]) {
                size = element.name;
                size_id = element.id;
            }
        });
        product_detail.color_details.forEach(element => {
            if(element.id == i["color"]) {
                color_id = element.id;
                color_name = element.name;
            }
        });
        html = '';
        html +=    '<div class="product-box-inner col-12 col-sm-12 col-md-12">';
        html +=        '<div class="product-box-content w-100">';
        response.data.influencer_product_images.forEach(function(el) {
            if(el.is_cover_pic==1){
                html +=  '<img src="'+ MEDIA_URL+el.image_path+'">'
            }
        });

        html +=            '<div class="box-content">'
        html +=                '<p>'+ product_detail.name +'</p>'
        html +=                '<h2>KSH '+ product_detail.price  +'</h2>'
        html +=                '<div class="dropdown-content">'
        if(product_detail.size_details!='' && product_detail.size_details != null ) {

            html +=                    '<div class="form-field">'
            html +=                        '<label>Size</label>'
            html +=                        '<div class="dropdown multi-select-options d-flex product-size">'
            html +=                            '<select class="size-'+i["id"]+'" name="size" onchange="updateSize('+i["product_id"] +','+ i["id"] + ');">'
            response.data.product_details.size_details.forEach(function(el) {
                html +=                                     '<option value="'+el.id+'" id="size'+i["id"]+'">'+ el.name +'</option>'
            });
            html +=                           '</select>'

            html +=                        '</div>'
            html +=                    '</div>'
        }
        if(product_detail.color_details !== null && product_detail.color_details!='' ) {
            html +=                    '<div class="form-field">'
            html +=                        '<label>Color</label>'
            html +=                        '<div class="dropdown multi-select-options d-flex product-color">'
            html +=                            '<select class="color-'+i["id"]+'" name="color" onchange="updateColor('+i["product_id"] +','+ i["id"] +');">'
            response.data.product_details.color_details.forEach(function(el) {
                html +=                                    '<option value="'+el.id+'"  id="color'+i["id"]+'">'+ el.name +'</option>'
            });
            html +=                            '</select>'

            html +=                        '</div>'
            html +=                    '</div>'
        }
        html +=                '</div>'
        html +=                 '<div class="quantity">'
        html +=                     'Qty' 
        html +=                     '<button class="pluse" onclick="updateQuantity('+ i["product_id"] +','+ product_detail.stock+','+ product_detail.price+',' + i["id"] + ', 0)">-</button>'
        html +=                      '<input type="text" id="quantity-text'+i["id"]+'" value="'+ i["quantity"] +'" disabled size="4">'
        html +=                     '<button class="min" onclick="updateQuantity('+ i["product_id"] +','+ product_detail.stock+','+ product_detail.price+',' + i["id"] +', 1)"> +</button>'
        html +=                     '<div id="stock-error'+i["product_id"]+'" style="color:red"></div>'
        html +=                     '<div class="cart-error" style="color:red"></div>'
        html +=                 '</div>'
        user_id && accessToken ? 
        html +=                '<div class="product-remove"><span onclick="deleteCartItemFromDB('+ i["id"] +')" class="remove-item"><img src="images/icons/Delete.svg" alt="">REMOVE</span></div>'
        : html +=                '<div ><span onclick="deleteCartItem('+i["id"] +')" class="remove-item"><img src="/images/icons/Delete.svg" alt="">REMOVE</span></div>'
        
        html +=            '</div>'
        if(response.product_reviews_and_ratings.length>0){
            html +=            '<div class="product-rating">'
            let averageRating;
            let totalRating = 0
            response.product_reviews_and_ratings.forEach(function(el) {
                totalRating = totalRating + el.rating;
            });
            averageRating = totalRating / response.product_reviews_and_ratings.length
            if (response.product_reviews_and_ratings.length == 0) {
                html +=                '<span class="rating-title">0</span>'
            } else {
            html +=                '<span class="rating-title">'+  Math.floor(averageRating)  +'</span>'
            }
            html +=                '<span class="rating-star"><img src="images/icons/star.svg" alt=""></span>'
            html +=            '</div>'
        }
        html +=        '</div>';
        html +=    '</div>';
        totalPrice += (parseInt(product_detail.price) * parseInt(i["quantity"]));
        setTimeout(function() {
            $('.size-'+i["id"]).find('option[value="'+ size_id+'"]').attr('selected', true)
            $('.color-'+i["id"]).find('option[value="'+ color_id+'"]').attr('selected', true)
        }, 1000);
    }

    $(".cart-box").append(html);
    $("#total").html(totalPrice);
    setTotalInLocalStorage(totalPrice);

}




function deleteCartItem(cart_id) {
    let check = confirm('Are you sure you want to delete this item');
    if(check === false) return;

    for (let i = 0; i < parsedCart.length; i++)
    {
        if (cart_id == parsedCart[i]["id"]) {
            parsedCart.splice(parsedCart[i], 1);
        }
    }
 
    setLocalStorage('cart', JSON.stringify(parsedCart));
    location.reload();
    updateCartIcon();
}

function deleteCartItemFromDB(cart_id) {
    let check = confirm('Are you sure you want to delete this item');
    if(check === false) return;
    __ajax_http("buyer/cart/delete/"+cart_id, "", headers(), AJAX_CONF.apiType.DELETE, "GET Sponsee", __success_delete);
}

function __success_delete(response) {
    console.log(response);
    if(response.status === true)
        location.reload();
}

function clearCart() {
    let check = confirm('Are you sure you want to clear the Cart');
    if(check === false) return;
    if(cart) {
        window.localStorage.removeItem('cart');
        window.localStorage.removeItem('total');
    }
    if (user_id && accessToken) {
        __ajax_http("buyer/cart/empty", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", "");
    }
    showEmptyCartPage();
    $(".product-box").html('');
    updateCartIcon();
    $("#total").html(total);
}

// $(document).ready(function(){
//     $("select.size").change(function(){
//         var selectedCountry = $(this).children("option:selected").val();
//         alert("You have selected the country - " + selectedCountry);
//     })
// })

function updateSize(product_id, cart_id) {
    console.log("clicked");
    let size = $(".size-"+cart_id).children("option:selected").val();
    let color = $(".color-"+cart_id).children("option:selected").val();
    for (let i = 0; i < parsedCart.length; i++)
    {
        if( parsedCart[i]["id"] == cart_id) {
            console.log("Matched");
            parsedCart[i]["size"] = size;
        }
    }
    window.localStorage.setItem('cart', JSON.stringify(parsedCart));
    if (user_id && accessToken) {
        var data = {
            "size": size,
            "quantity": $("#quantity-text"+cart_id).val(),
            "color" : color
        }
        console.log(data);
        updateCartPage(data, cart_id);
    }
    // $('.size-'+cart_id).find('option[value="'+size+'"]').attr('selected', true);
}

// Update cart data in database
function updateCartPage(data, cart_id) {
    __ajax_http("buyer/cart/update/"+cart_id, data, headers(), AJAX_CONF.apiType.PUT, "GET Sponsee", __success_update_cart_page);
}

function __success_update_cart_page(response) {
    console.log(response);
    if(response.status === true) {
        $('#checkout-btn').attr("disabled", false);
        $('.cart-error').html("");
    }
    if(response.status === false) {
        $('#checkout-btn').attr("disabled", true);
        $('.cart-error').html(response.message);
    }
}

function updateColor(product_id, cart_id=null) {
    console.log("clicked");
    let color = $(".color-"+cart_id).children("option:selected").val();
    console.log($(".size-"+cart_id).val());
    if (user_id && accessToken) {
        var data = {
            "size": $(".size-"+cart_id).val(),
            "quantity": $("#quantity-text"+cart_id).val(),
            "color" : color
        }
        updateCartPage(data, cart_id);
    } else {
        for (let i = 0; i < parsedCart.length; i++)
        {
            if( parsedCart[i]["id"] === cart_id)
            console.log(cart_id);
                parsedCart[i]["color"] = color;
        }
        window.localStorage.setItem('cart', JSON.stringify(parsedCart));
    }
    // $(".color-"+cart_id).val(color);
}
// function getCartDataFromDB() {
//     __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __cartPage);
// }

// async function fetchData(url){
//     return await fetch(url,{
//         headers:{
//             'Content-Type': 'application/json',
//             ['Authorization']: "Bearer " + STORAGE.get(STORAGE.accesstoken),
//         }
//     }).then(r => r.json());
// }

// async function fetchQuantity() {
//     return  await fetchData(API_URL+ 'buyer/cart/items/listing')
// }

function updateQuantity(product_id, stock, price, cart_id, flag) {
    let total = parseInt(document.getElementById("total").innerHTML);
    var qty = document.getElementById("quantity-text"+cart_id).value;
    flag == 1 ? qty++ : qty--;
    var cart = window.localStorage.getItem('cart') || '[]';
    // let product_quantity_in_cart = 0;
    // let cartDetails;
    // if(user_id && accessToken) {
    //     cartDetails = await fetchQuantity();
    //     cartDetails = cartDetails.data;
    // } else {
    //     cartDetails = JSON.parse(cart);
    // }
    // console.log(cartDetails);
    // cartDetails.forEach(p => {
    //     if(p["product_id"] == product_id) {
    //         console.log(p["quantity"])
    //         product_quantity_in_cart += p["quantity"]
    //     }
    // })
    // console.log(product_quantity_in_cart, stock);

    // if(flag== 1 && product_quantity_in_cart >= stock) {
    //     $("#stock-error"+product_id).html("Out Of Stock");
    //     // $('#checkout-btn').attr("disabled", true);
    //     return;
    // }

    // stock quantity is 0
    if(qty == 0) {
        user_id && accessToken ? deleteCartItemFromDB(cart_id) : deleteCartItem(cart_id);
        return;
    }
    // stock  is less than qty
    if(qty > stock) {
        $("#stock-error"+product_id).html("Out Of Stock");
        $('#checkout-btn').attr("disabled", true);
        return;
    }
    // qty is equal to or greater than stock 
    if(qty <= stock) {
        $("#stock-error"+product_id).html(" ");
        $('#checkout-btn').attr("disabled", false);
    }

    if (user_id && accessToken) {
        var data = {
            "size": $(".size-"+cart_id).val(),
            "quantity": qty,
            "color" : $(".color-"+cart_id).val()
        }
        console.log(data);
        updateCartPage(data, cart_id);
    } else {
        for (let i = 0; i < parsedCart.length; i++)
        {
            if( parsedCart[i]["id"] === cart_id)
                parsedCart[i]["quantity"] = qty;
        }
        setLocalStorage('cart', JSON.stringify(parsedCart));

    }

   

    document.getElementById("quantity-text"+cart_id).value = qty;
    flag == 1 ? total += price : total -= price;
    setTotalInLocalStorage(total);
    $("#total").html(total);

}

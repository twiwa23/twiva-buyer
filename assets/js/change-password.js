function checkPasswordLength() {
    var new_password = $("#new_password").val();
    if (new_password.length <= 7) {
        $("#new_password_check").html("Password must be at least 8 characters").css("color", "red");
        $("#updatePassword").attr("disabled", true);
    } else {
        $("#new_password_check").html("");
        $("#updatePassword").attr("disabled", false);
    }
}

function checkCurrentPasswordLength() {
    var current_password = $("#current_password").val();
    if (current_password.length > 1) {
        $("#current_password_check").html("");
    }
}

function checkPasswordMatch() {
    var new_password = $("#new_password").val();
    var confirm_password = $("#confirm_password").val();
    if (new_password != confirm_password) {
        $("#confirm_password_check").html("Passwords does not match!").css("color", "red");
        $("#updatePassword").attr("disabled", true);
    } else {
        $("#confirm_password_check").html("Passwords match.").css("color", "green");
        $("#updatePassword").attr("disabled", false);
    }
}

function validatePassword() {
    let current_password = $("#current_password").val();
    let new_password = $("#new_password").val();
    let confirm_password = $("#confirm_password").val();
    if(current_password.replace(/\s+/g, '') == '') {
        $("#current_password_check").html("Please Enter the current Password.").css("color", "red");
        return;
    }
    if(new_password.replace(/\s+/g, '') == '') {
        $("#new_password_check").html("Please Enter the New Password.").css("color", "red");
        return;
    }
    if(confirm_password.replace(/\s+/g, '') == '') {
        $("#confirm_password_check").html("Please Enter the Current Password.").css("color", "red");
        return;
    }
    if (new_password != confirm_password) {
        $("#confirm_password_check").html("Passwords does not match!").css("color", "red");
        $("#updatePassword").attr("disabled", true);
        return
    }
    if (new_password.length <= 7) {
        $("#new_password_check").html("Password must be at least 8 characters").css("color", "red");
        $("#updatePassword").attr("disabled", true);
        return
    }

    let data = {
        "current_password": current_password,
        "new_password": new_password,
        "confirm_password": confirm_password
    }

    updatePassword(data);

}

function updatePassword(data) {
    __ajax_http("influencer/change/password?", data, headers(), AJAX_CONF.apiType.POST, "", __success_update_password);
}

function __success_update_password(response) {
    console.log(response);
    if(response.status == true) {
        window.localStorage.setItem("message", "Password Changed Successfully");
        location.href = 'buyer-account-settings.php';
    }
    if(response.status == false) {
        $(".alert-danger").show().html(response.error);
        $(".alert-danger").show().html(response.message);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}

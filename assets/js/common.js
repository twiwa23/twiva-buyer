// Twiva Buyer Common JS


// Variables
var cart = window.localStorage.getItem('cart') || '[]';
if(cart) {
    var parsedCart = JSON.parse(cart)
    if(parsedCart.length == 0){
        window.localStorage.removeItem('total');
    }
}

var user_id = window.localStorage.getItem('userId');
var accessToken = localStorage.getItem(STORAGE.accesstoken);

// Total in all the pages
let total = window.localStorage.getItem('total');
total = JSON.parse(total)
if(!total) {
    $(".order-summary").hide();
    $(".back-link").hide();
}
if(total) {
    $(".order-summary").show();
    $("#total").html(total);
}
$("#total").html(total);

// Functions to Call on Load
// updateCartIcon();
var carts =[]; // To store Cart Table Data


if(user_id && accessToken) {

    //Dont Show Login/ Signup page if already logged in
    if (location.pathname == UI_URL.buyerLogin || location.pathname == UI_URL.buyerSignup) {
        goto(UI_URL.buyerCart);
    }

    // Send Localstorage cart to Database
    // try {
    //     storeCartData(user_id)
    // } catch(err) {
    //     console.log(err);
    // }

}



function updateCartOnLogin(data, cart_id) {
    __ajax_http("buyer/cart/update/"+cart_id, data, headers(), AJAX_CONF.apiType.PUT, "GET Sponsee", __success_update_cart_on_login);
}

function __success_update_cart_on_login(response) {
    console.log(response);
    if(response.status === false) {
        console.log(response.message ?? response.error);
    }
    if(response.status === true) {
        window.localStorage.removeItem('cart');
        __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", (res)=> {
            let cartTotal = 0;
            res.data.forEach(ele => {
                cartTotal += (parseInt(ele.product_price) * parseInt(ele.quantity));
            });
            if(document.getElementById("total")) { document.getElementById("total").innerHTML = cartTotal }
            window.localStorage.setItem("total", cartTotal);
            console.log(cartTotal);
            document.getElementById("cart-count").innerHTML =  res.data.length;
            if(location.pathname == '/buyer-shipping.php') {
                console.log(res.data.length)
                if(res.data.length == 0)
                    goto(UI_URL.buyerOrders);
            }
        });
        // updateCartIcon();
    }
} 


function userCartOnLogin(data) {
    console.log(data);
    __ajax_http("buyer/cart/add", data, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __user_cart_success_on_login);
}

function __user_cart_success_on_login(response) {
    console.log(response);
    if(response.status === false) {
        console.log(response.message);
    }
    if(response.status === true) {
        window.localStorage.removeItem('cart');
        __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", (res)=> {
            let cartTotal = 0;
            res.data.forEach(ele => {
                cartTotal += (parseInt(ele.product_price) * parseInt(ele.quantity));
            });
            if(document.getElementById("total")) { document.getElementById("total").innerHTML = cartTotal }
            window.localStorage.setItem("total", cartTotal);
            console.log(cartTotal);
            document.getElementById("cart-count").innerHTML =  res.data.length;
            if(location.pathname == '/buyer-shipping.php') {
                console.log(res.data.length)
                if(res.data.length == 0)
                    goto(UI_URL.buyerOrders);
            }
        });
    }
    
}



// To updateCartIcon

function updateCartIcon() {
    if ( user_id && accessToken) {
        cartCount();
    } else {
        document.getElementById("cart-count").innerHTML =  parsedCart.length ?? 0;
    }
}

// Get Items From Db
function cartCount() {
    __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_cartCount);
}

function __success_cartCount(response) {
    carts = response.data;
    document.getElementById("cart-count").innerHTML =  response.data.length;
    if(parsedCart && parsedCart.length !== 0) {
        storeCartData(carts)
    } else {
        checkCart();
    }
}

function checkCart() {
    __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_check_cart);
}

function __success_check_cart(response) {
    console.log(response);
    if(location.pathname == '/buyer-shipping.php') {
        console.log(response.data.length)
        if(response.data.length == 0)
            goto(UI_URL.buyerOrders);
    }
}


function storeCartData(carts) {

    parsedCart.forEach(ele => { // 4
        let item = carts.filter(element => element.product_id === ele.product_id );
        if(item.length !== 0) {
            console.log(item);
            for (let i of item) {
                console.log(i.size , ele.size);
                if ((i.size && ele.size == i.size) &&  (i.color && ele.color == i.color) || i.size=="" || i.color == "") {
                    console.log("Found");
                    var data = {
                        "size": ele.size,
                        "quantity": parseInt(i["quantity"]) + parseInt(ele.quantity),
                        "color" : ele.color
                    }
                    
                    updateCartOnLogin(data, i['id']);
                } else {
                    let json = {"items" : [{
                        "buyer_id": user_id,
                        "influencer_id": ele.influencer_id,
                        "business_id": ele.business_id,
                        "product_id": ele.product_id,
                        "quantity":ele.quantity,
                        "size": ele.size,
                        "color": ele.color
                    }]};
                    userCartOnLogin(json);
                }
            }
            
        } else {
            let json = {"items" : [{
                "buyer_id": user_id,
                "influencer_id": ele.influencer_id,
                "business_id": ele.business_id,
                "product_id": ele.product_id,
                "quantity":ele.quantity,
                "size": ele.size,
                "color": ele.color
            }]};
            userCartOnLogin(json);
        }
    })

    


    // cartCountUpdate(res => {
    //     if(document.getElementById("total")) { document.getElementById("total").innerHTML = calCartTotal(res) }
    //     window.localStorage.setItem("total", calCartTotal(res));
    //     console.log(calCartTotal(res));
    //     document.getElementById("cart-count").innerHTML =  res.data.length;
    //     if(location.pathname == '/buyer-shipping.php') {
    //         console.log(res.data.length)
    //         // if(res.data.length == 0)
    //             // goto(UI_URL.buyerOrders);
    //     }
    // });

}


function cartCountUpdate(cb){
    __ajax_http("buyer/cart/items/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", cb);
}

function calCartTotal(res) {
    let cartTotal = 0;
    res.data.forEach(ele => {
        cartTotal += (parseInt(ele.product_price) * parseInt(ele.quantity));
    });
    return cartTotal;
}


// Show Empty Cart Page

function showEmptyCartPage() {
    let empty_html = '';
    empty_html += '<div class="empty-wrapper w-100">'
    empty_html +=   '<div class="text-center w-100 ">'
    empty_html +=       '<img src="images/icons/empty.svg" alt="">'
    empty_html +=    '</div>'
    empty_html +=    '<p class="text-center">Your Cart is Empty . </p>'
    empty_html +=    '<div class="btn-wrapper text-center mt-3">'
    // empty_html +=        '<a href="#" class="white-bttn p-2 disply-inline-block">Back to product page</a>'
    empty_html +=    '</div>'
    empty_html += '</div>'
    $(".empty_cart").append(empty_html);
    $(".checkout-section").hide();
    $(".cart-clear").hide();
}

function setTotalInLocalStorage(value)
{
    setLocalStorage('total', value);
}

function setLocalStorage(key, value) {
    window.localStorage.setItem(key, value);
}

function getLocalStorage(key) {
    window.localStorage.getItem(key);
}

function removeLocalStorage(key) {
    window.localStorage.removeItem(key);
}


async function fetchData(url){
    return await fetch(url,{
        headers:{
            'Content-Type': 'application/json',
            ['Authorization']: "Bearer " + STORAGE.get(STORAGE.accesstoken),
        }
    }).then(r => r.json());
}

async function fetchQuantity() {
    return  await fetchData(API_URL+ 'buyer/cart/items/listing')
}
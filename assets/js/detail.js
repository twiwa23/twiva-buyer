//
//#detail js
//\

window.addEventListener('DOMContentLoaded', function (event) {

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const inf_id = urlParams.get('in')
    // console.log(inf_id);
// shirt

    const pid = urlParams.get('pid')
    // console.log(pid);
// blue
    if(pid && inf_id) {
        getProductDetails(inf_id,pid) 
    }
});

    function getProductDetails(inf_id,pid){
        __ajax_http("buyer/"+ inf_id +"/"+ pid+"/details", "", { 'Accept': 'application/json' }, AJAX_CONF.apiType.GET, "GET Sponsee", __success_product_details);
        
    }
    function __success_product_details(response){
        console.log(response);
        let coverPicHtml = '';
        let coverImagesHtml = '';
        if(response.data.product_details.stock == 0) {
            $(".add-to-cart-btn").html("Out Of Stock");
            $(".add-to-cart-btn").attr("disabled", true);
        }
        
        response.data.influencer_product_images.forEach(function(el) {
            if(el.is_cover_pic==1){
                coverPicHtml +=        '<div class="carousel-item active" id="slide-'+ el.id +'" data-slide-number="'+ el.id +'">'
                coverPicHtml +=            '<img src="'+MEDIA_URL+el.image_path +'" class="d-block w-100" alt="..." data-remote="'+ MEDIA_URL+el.image_path +'" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">'
                coverPicHtml +=        '</div>'
                coverImagesHtml +=     '<div id="carousel-selector-'+ el.id +'" class="thumb  selected" data-target="#myCarousel" data-slide-to="0">'
                coverImagesHtml +=          '<img src="'+MEDIA_URL+el.image_path +'" class="img-fluid" alt="...">'
                coverImagesHtml +=      '</div> '
                // $("#cover-pic").attr("src",MEDIA_URL+el.image_path);
            } else {
                coverPicHtml +=        '<div class="carousel-item" id="slide-'+ el.id +'" data-slide-number="'+ el.id +'">'
                coverPicHtml +=            '<img src="'+ MEDIA_URL+el.image_path +'" class="d-block w-100" alt="..." data-remote="'+ MEDIA_URL+el.image_path +'" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">'
                coverPicHtml +=        '</div>'

                coverImagesHtml +=      '<div id="carousel-selector-'+ el.id +'" class="thumb " data-target="#myCarousel" data-slide-to="'+ el.id +'">'
                coverImagesHtml +=           '<img src="'+ MEDIA_URL+el.image_path +'" class="img-fluid" alt="...">'
                coverImagesHtml +=      '</div>'
            } 
            // $(".more-images").append(images);
        });

        $(".inner-carousel").append(coverPicHtml);
        $(".row .mx-0").append(coverImagesHtml);

        $('[id^=carousel-selector-]').click(function(e) {
            var id_selector = $(this).attr('id');
            var id = parseInt( id_selector.substr(id_selector.lastIndexOf('-') + 1) );
            $('[id^=carousel-selector-]').removeClass('selected');
            $('[id=carousel-selector-'+id+']').addClass('selected');
            $('[id^=slide-]').removeClass('active');
            $('[id=slide-'+id+']').addClass('active');
        });

        var product_detail= response.data.product_details;
        window.product_id = response.data.id;
        window.influencer_id = response.data.influencer_id;
        window.business_id = response.data.product_details.user_id;
        window.stock = parseInt(response.data.product_details.stock);
        $(".title").html(product_detail.name);
        $(".price").html("KSH "+ product_detail.price);
        $(".description").html(product_detail.description);
        
        var sizeHTML='';
        var specificationHTML='';
        var reviewHTML='';

        if(product_detail.specification!=null && product_detail.specification !=''){
            let specification = product_detail.specification.split(",");
            specification.forEach(element => {
                specificationHTML += '<li>'+ element + '</li>'
            });
        }

        if(response.product_reviews_and_ratings!=null && response.product_reviews_and_ratings !=''){
            response.product_reviews_and_ratings.forEach(element => {
                if(element.buyer_name == null) {
                    buyer_name = 'Buyer';
                } else {
                    buyer_name = element.buyer_name.name
                }
                reviewHTML+=    '<div class="panel-body">'
                reviewHTML+=        '<div class="review-author">'
                reviewHTML+=            '<span><img src="images/icons/verified.svg"></span>'+ buyer_name
                reviewHTML+=        '</div>'
                reviewHTML+=        '<div class="date">'
                // reviewHTML+=            '<img src="images/icons/star-rate.svg">'
                reviewHTML+=            '<div class="rating">'
                let gold = element.rating;
                let empty = 5 - element.rating;
                for(let i=0; i< gold; i++) {
                    reviewHTML+=                   '<i class="fa fa-star"></i>'
                }
                for(let j=0; j< empty; j++) {
                    reviewHTML+=                   '<i class="fa fa-star-o"></i>'
                }   
                let date  = element.created_at.split('T')[0];
                date = new Date(date)
                reviewHTML+=          '<span class="r-date">' + date.toDateString().split(' ').slice(1).join(' '); + '<span>'
                reviewHTML+=            '</div>'
                
                reviewHTML+=        '</div>'
                reviewHTML+=        '<p>' + element.review
                reviewHTML+=        '</p>'
                reviewHTML+=    '</div>'
                reviewHTML+=    '<hr/>'
            });
            $(".rating-panel").html(reviewHTML)
        } else {
            reviewHTML="<a>No Review & Ratings Yet.</a>";
            $(".rating-title").html(reviewHTML);
        }


        if(specificationHTML==''){
            specificationHTML="No specification mentioned.";
        }

        $(".specification").html(specificationHTML);

        if(product_detail.size_details == null || product_detail.size_details == ''){
            $(".size-row").hide();
        }
        if(product_detail.color_details == null || product_detail.color_details == ''){
            $(".color-row").hide();
        }

        product_detail.size_details.forEach(function(el) {
            sizeHTML +=    '<input type="radio" name="size" id="sm-size-'+el.name+'" class="size-input" value='+el.id+' required>'
            sizeHTML +=    '<label for="sm-size-'+el.name+'" class="size-list-item">'+el.name+'</label>'
        });
        $(".size-list").html(sizeHTML);

        var colorHTML='';
        product_detail.color_details.forEach(function(el) {
            colorHTML += '<input type="radio" name="color" id="color-box-'+el.name+'" class="color-input" value='+el.id+' required>'
            colorHTML +=     '<label for="color-box-'+el.name+'" class="color-list-item" id="color-1-'+el.name+'" style="background:#'+el.hexaval+'">'
            colorHTML +=         '<img src="./images/icons/Vector-right.svg" alt="">'
            colorHTML +=      '</label>'
        });
        $(".color-list").html(colorHTML);

    }
    
    var array = []; 
    
    // async function fetchData(url){
    //     return await fetch(url,{
    //         headers:{
    //             'Content-Type': 'application/json',
    //             ['Authorization']: "Bearer " + STORAGE.get(STORAGE.accesstoken),
    //         }
    //     }).then(r => r.json());
    // }
    
    // async function fetchQuantity() {
    //     return  await fetchData(API_URL+ 'buyer/cart/items/listing')
    // }
    
    function addToCart() {
        const params = new URLSearchParams(window.location.search)
            // add validation to check pid and quantity 
        let product_id = params.get('pid')
        let influencer_id = params.get('in')
        let quantity = document.getElementById("pid_quantity") ? document.getElementById("pid_quantity").value : 1
        let product_quantity_in_cart = 0;
        let i;
	
        var size = document.getElementsByName('size') ?? '';
        var color = document.getElementsByName('color') ?? '';
        if(color.length==0){
            color = '';
        }
        if(size.length==0){
            size = '';
        }
        console.log(color);
        for (i of size) {
            if(i.checked) {
                size = i.value;
            }
        }
        for (i of color) {
            if(i.checked) {
                color = i.value;
            }
        }


       /* if(typeof size === 'object' && size !== null) {
            alert("Please Select Size");
            return;
        }*/
        /*if(typeof color === 'object' && color !== null) {
            alert("Please Select Color");
            return;
        }*/

        // if(user_id && accessToken) {
        //     cartDetails = await fetchQuantity();
        //     cartDetails = cartDetails.data;
        // } else {
        //     cartDetails = JSON.parse(cart);
        // }
        // console.log(cartDetails);
        // cartDetails.forEach(p => {
        //     if(p["product_id"] == window.product_id) {
        //         console.log(p["quantity"])
        //         product_quantity_in_cart += p["quantity"]
        //     }
        // })
        // console.log(product_quantity_in_cart, window.stock);

        // if(product_quantity_in_cart >= window.stock) {
        //     $("#error").html("Out Of Stock, Already items in cart").css("color", "red");
        //     $('.add-to-cart-btn').attr("disabled", true);
        //     return;
        // }

        if (user_id && accessToken) {
            // console.log(carts);

            let user_id = window.localStorage.getItem('userId');
            if(carts && carts.length !== 0) {

                let found = carts.find(element => element.product_id == window.product_id);
                console.log(found);
                if(found) {
                    // if((size && element.size == size) &&  (color && element.color == color) || element.size == "" || element.color == "") {
                    let json = {
                        "size": size,
                        "quantity": (found["quantity"] + quantity),
                        "color" : color
                    }
                    updateCart(json, found["id"]);
                } else {
                    let data = {"items" : [{
                        "buyer_id": user_id,
                        "influencer_id": window.influencer_id,
                        "business_id":window.business_id,
                        "product_id": window.product_id,
                        "quantity": quantity,
                        "size": size,
                        "color": color
                    }]};
                    console.log(data);
                    console.log("Adding Product to Cart");
                    userCart(data);
                }
            } else {
                let data = {"items" : [{
                    "buyer_id": user_id,
                    "influencer_id": window.influencer_id,
                    "business_id":window.business_id,
                    "product_id": window.product_id,
                    "quantity": quantity,
                    "size": size,
                    "color": color
                }]};
                console.log(data);
                userCart(data);
            }
        } else {

            let isAlreadyExist = false;

            for (i = 0; i < parsedCart.length; i++)
            {
                if (parsedCart[i]['product_id'] ===  window.product_id) {
                    // if((size && parsedCart[i]["size"] == size) &&  (color && parsedCart[i]["color"] == color)  || parsedCart[i]["size"] == "" || parsedCart[i]["color"] == "") {
                        console.log(parsedCart[i]['product_id'], product_id);
                        parsedCart[i]['quantity'] += quantity
                        isAlreadyExist = true;
                        break;
                    // }
                }
            } 

            if (isAlreadyExist == false) {
                console.log(isAlreadyExist);
                parsedCart.push({
                    "id" : window.product_id + Math.floor(Math.random() * 100),
                    "buyer_id":"",
                    "influencer_id":window.influencer_id,
                    "inf_id": influencer_id, //encrypted id
                    "business_id": window.business_id,
                    "product_id": window.product_id, 
                    "pid" : product_id, // encrypted id
                    "quantity": quantity,
                    "size": size,
                    "color": color
                })
            }
            window.localStorage.setItem('cart', JSON.stringify(parsedCart));
            updateCartIcon();
            location.href = '/buyer-cart.php';
        }
    }

    function userCart(data) {
        console.log(data);
        __ajax_http("buyer/cart/add", data, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __user_cart_success);
    }

    function __user_cart_success(response) {
        console.log(response);
        updateCartIcon();
        // cartCountUpdate(res => {
        //     document.getElementById("total").innerHTML = calCartTotal(res);
        //     window.localStorage.setItem("total", calCartTotal(res));
        //     console.log(calCartTotal(res));
        //     document.getElementById("cart-count").innerHTML =  res.data.length;
        // });
        location.href = '/buyer-cart.php';
    }

    // Update cart data in database
function updateCart(data, cart_id) {
    __ajax_http("buyer/cart/update/"+cart_id, data, headers(), AJAX_CONF.apiType.PUT, "GET Sponsee", __success_updateCart);
}

function __success_updateCart(response) {
    console.log(response);
    updateCartIcon();
    location.href = '/buyer-cart.php';
}

window.addEventListener('DOMContentLoaded', function (event) {
    if (user_id && accessToken) {
        const params = new URLSearchParams(window.location.search)
        let product_id = params.get('rid')
        if(product_id) {
            fetchReview(product_id);
        } else {
            localStorage.removeItem('title');
        }
    }
});

function fetchReview(product_id) {
    __ajax_http("buyer/reviews/fetch/"+product_id, "", headers(), AJAX_CONF.apiType.GET,"", __success_fetchReview);
}

function __success_fetchReview(response) {
    console.log(response);
    $(".review-product-title").html(window.localStorage.getItem("title"))
    $(".review-product-image").attr('src', MEDIA_URL + window.localStorage.getItem("image"))
    $("#review").val(response.data.review);
    $("#review_id").val(response.data.id);
    $("input[name=stars][value=" + response.data.rating + "]"). prop('checked', true);
}

function updateReview(){
    const params = new URLSearchParams(window.location.search)
    let product_id = params.get('rid')
    let review_id = $("#review_id").val()
    let data = {
        "product_id": product_id,
        "rating": $('input[name="stars"]:checked').val(),
        "review":$("#review").val()
    }
    console.log(data);
    updateReviewApi(data, review_id);
}

function updateReviewApi(data, review_id) {
    __ajax_http("buyer/reviews/update/"+review_id, data, headers(), AJAX_CONF.apiType.PUT,"", __success_update_review);
}

function __success_update_review(response) {
    console.log(response);
    if(response.status === true) {
        let order_id = window.localStorage.getItem("oid");
        window.localStorage.setItem("message", "Thanks For Giving your Review");
        if(order_id){
            location.href = 'buyer-order-detail.php?oid='+order_id;
        }
        else{
            window.history.back();
        }
    }
    if(response.status === false) {
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}


// Check Login Status before Proceeding to checkout
function checkLoginStatus() {
    let total = parseInt(document.getElementById("total").innerHTML);
    window.localStorage.setItem('total', JSON.stringify(total));
    $("#total").html(total);
    updateCartIcon();

    if (!user_id || !accessToken) {
        goto(UI_URL.buyerLogin);
    } else {
        goto(UI_URL.buyerShipping);
    }

}

// REgister With Email & password & with Default Data
function signup() {
    let name = $("#name").val();
    let password = $("#password").val();
    if(name.length < 3) {
        $("#name_error").html("Name must be of atleast 3 Characters").css("color", "red");
        return;
    }
    if(password.length < 8) {
        $("#password_error").html("Password must be of atleast 8 Characters").css("color", "red");
        return;
    }
    var json = {
        "name" : name,
        "email": $("#email").val(),
        "password": password,
        "type" : 3,
        "dob":"13-03-1995",
        "gender":"1",
        "referal_code" : "",
        "description":"abc",
        "interests":"1,2,3",
        "profile_image":"abc.png"
    };
    try {
        register(json);
    } catch(err) {
        console.log(err);
    }
}

function register(json) {
    __ajax_http("register/influencer", json, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __success_signup);
}

function __success_signup(response) {
    console.log(response);
    if (response.status != true) {
        if(response.message){
             $("#usercheck").show().css("color","red").html(response.message);
        }
        return; 
    }


    if(response.status === true) {
        var verify = window.localStorage.getItem('verify');
        verify = JSON.parse(verify);
        if(verify) {
            window.localStorage.removeItem('verify')
        }
        window.localStorage.setItem('verify', JSON.stringify({
            "otp" :  response.otp,
            "email" : response.email
        }));
        goto(UI_URL.otpVerify);
    }

}

function verifyOtp() {
    var data = window.localStorage.getItem('verify') || [];
    data = JSON.parse(data);

    if(data.length = 0) {
        return;
    }
    console.log(data['otp'], parseInt($("#otp").val()));
    if(data['otp'] != parseInt($("#otp").val())) {
        console.log("Otp not matched");
        $("#otp-error").html("Otp not matched").css("color", "red");
        return;
    } 
    var json = {
        "otp" : $("#otp").val(),
        "email" : data['email'],
        "device_token":"fdsffsdfsfs",
        "device_type":"3"
    }
    console.log("OTP Matched");
    otpVerify(json);

}

function otpVerify(json) {
    __ajax_http("verify/otp", json, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __success_verifyOtp);
}

function __success_verifyOtp(response) {
    console.log(response);
    if (response.status == true) {
        STORAGE.set(STORAGE.accesstoken, response.token);
        STORAGE.set(STORAGE.userId, response.data.id);
        setTimeout(function() {
            goto(UI_URL.buyerShipping);
        }, 100);
    } else {
        console.log(response.message);
        $("#otp-error").html(response.message).css("color", "red");
    }
    
}

// To Resend the OTP 
function reVerify() {
    goto(UI_URL.reVerifyEmail);
}

function resendEmailOTP() {
    let data = {
        email : $("#forgot_password_mail_id").val()
    };
    console.log(data);
    resendOTP(data);
}

function resendOTP(data) {
    __ajax_http("resend/otp", data, headers(), AJAX_CONF.apiType.POST, "GET Sponsee", __success_resend_email_otp);
}

function __success_resend_email_otp(response) {
    console.log(response)
    if(response.status === true) {
        window.localStorage.setItem('verify', JSON.stringify({
            "otp" :  response.otp,
            "email" : response.email
        }));
        goto(UI_URL.otpVerify);
    }
}

function login() {
    let email = $("#email").val();
    let password = $("#password").val();
    if(email.replace(/\s+/g, '') == '') {
        $("#emailError").show().css("color","red").html("Please Enter the Email.");
        return;
    }
    if(password.replace(/\s+/g, '') == '') {
        $("#emailError").show().css("color","red").html("Please Enter the Password.");
        return;
    }
    var json = {
        "email": $("#email").val(),
        "password": $("#password").val(),
        "device_type": "3",
        "device_token": "sadsadasd",
    }
    // console.log(json);
    checkEmailPassword(json);
}

function checkEmailPassword(json) {
    __ajax_http("login/influencer", json, { 'Accept': 'application/json' }, AJAX_CONF.apiType.POST, "GET Sponsee", __success_login);
}

function __success_login(response) {
    console.log(response);
    if (response.status === false) {
        if(response.error){
             $("#emailError").show().css("color","red").html(response.error);
        }
        if(response.error == "Please verify your email.") {
            $("#verify_email_button").show();
        }
        if(response.message){
            $("#emailError").show().css("color","red").html(response.message);
       }
        return; 
    }
      
    __save_identity(response);
	if(response.is_verified==0){
		 goto(UI_URL.otpVerify);

	}
}

function __save_identity(response) {
    // console.log(response);
	
  //  STORAGE.set(STORAGE.accesstoken, response.token);
//    STORAGE.set(STORAGE.userId, response.data.id);
    window.localStorage.removeItem('verify');
	if(response.is_verified==0){

        window.localStorage.setItem('verify', JSON.stringify({
            "otp" :  response.otp,
            "email" : response.email
        }));
	setTimeout(function() {
        goto(UI_URL.otpVerify);
    }, 100);

	}
	else{

        //goto(UI_URL.otpVerify);
 STORAGE.set(STORAGE.accesstoken, response.token);
    STORAGE.set(STORAGE.userId, response.data.id);
    // cartCountUpdate(res => {
    //     res.data.length == 0 ? goto(UI_URL.buyerOrders) : goto(UI_URL.buyerShipping);
    // });
    setTimeout(function() {
        goto(UI_URL.buyerShipping);
    }, 100);
	}
}


function validateEmailID() {
    var mailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var email = $("#forgot_password_mail_id").val()
    if(email.match(mailFormat)) {
        $("#email_check").html("");
        $("#send_link").attr("disabled", false);
    } else {
        $("#email_check").html("Please Enter correct Mail ID").css("color", "red");
        $("#send_link").attr("disabled", true);
    }
}



function forgotPassword() {
    var email = $("#forgot_password_mail_id").val()
    if(email.replace(/\s+/g, '') == '') {
        $("#email_check").html("Please Enter the email.").css("color", "red");
        return;
    }
    let data = {
        "email" : email
    };
    console.log(data);
    try {
        sendPasswordResetLink(data);
    } catch(err) {
        console.log(err);
    }
}

function sendPasswordResetLink(data) {
    __ajax_http("forgot-password/otp", data, { 'Accept': 'application/json' }, AJAX_CONF.apiType.POST, "GET Sponsee", __success_reset_password_link);
}

function __success_reset_password_link(response) {
    console.log(response)
    if(response.success == 200) {
        console.log(response.user.email);
        STORAGE.set(STORAGE.email, response.user.email);
        STORAGE.set(STORAGE.otp_id, response.user.otp_id);
        goto(UI_URL.reset_verify_link);
        return;
    }
    if(response.code == 400) {
        $("#email_check").html(response.message);
    }
    STORAGE.set(STORAGE.email, response.failure.user.email);
    STORAGE.set(STORAGE.otp_id, response.failure.user.otp_id);
    $("#email_check").html(response.failure.message);
    console.log("Email Not Found");
    
    
}

function verifyResetOtp() {
    var json = {
        "email":localStorage.getItem(STORAGE.email),
        "otp_id":localStorage.getItem(STORAGE.otp_id),
        "otp": $("#otp").val(),
    };
    console.log(json);
    otpResetVerify(json);
}


function otpResetVerify(json) {

    __ajax_http("forgot-password/Otp-verify", json, headers(), AJAX_CONF.apiType.POST, "POST LOGIN", __success_otp_verify);

}

function __success_otp_verify(response){
    console.log(response);
    if (response.code === 400) {
        $("#error").html(response.message).css("color", "red");
        console.log('Error')
      }
    if(response.code == 200) {
        goto(UI_URL.buyerResetPassword);
        return;
    }
}


function checkPasswordLength() {
    var new_password = $("#new_password").val();
    if (new_password.length <= 7) {
        $("#new_password_check").html("Password must be at least 8 characters").css("color", "red");
        $("#resetPasswordCheck").attr("disabled", true);
    } else {
        $("#new_password_check").html("");
        $("#resetPasswordCheck").attr("disabled", false);
    }
}

function checkPasswordMatch() {
    var new_password = $("#new_password").val();
    var confirm_password = $("#confirm_password").val();
    if (new_password != confirm_password) {
        $("#confirm_password_check").html("Passwords does not match!").css("color", "red");
        $("#resetPasswordCheck").attr("disabled", true);
    } else {
        $("#confirm_password_check").html("Passwords match.").css("color", "green");
        $("#resetPasswordCheck").attr("disabled", false);
    }
    if (new_password.length <= 7) {
        $("#new_password_check").html("Password must be at least 8 characters").css("color", "red");
        $("#resetPasswordCheck").attr("disabled", true);
    }
}

function resetPasswordCheck() {
    let new_password = $("#new_password").val();
    let confirm_password = $("#confirm_password").val();
    if(new_password.replace(/\s+/g, '') == '') {
        $("#new_password_check").html("Please Enter the New Password.").css("color", "red");
        return;
    }
    if(new_password.replace(/\s+/g, '') < 8) {
        $("#resetPasswordCheck").attr("disabled", true);
        return;
    }
    if(confirm_password.replace(/\s+/g, '') == '') {
        $("#confirm_password_check").html("Please Enter the Current Password.").css("color", "red");
        return;
    }
    if (new_password != confirm_password) {
        $("#confirm_password_check").html("Passwords does not match!").css("color", "red");
        $("#resetPasswordCheck").attr("disabled", true);
        return
    } else {
        $("#confirm_password_check").html("Passwords match.").css("color", "green");
        $("#resetPasswordCheck").attr("disabled", false);
    }
    var json = {
        "password": $("#new_password").val(),
        "otp_id": localStorage.getItem(STORAGE.otp_id),
        "email": localStorage.getItem(STORAGE.email),

        // "c_password": $("#c_password").val(),

    };
    // console.log(json);
    changePassword(json);
}

function changePassword(json) {
    __ajax_http("forgot-password/verify/otp", json, {'Accept': 'application/json'}, AJAX_CONF.apiType.POST, "POST LOGIN", __success_password_change);

}

function __success_password_change(response) {
    console.log(response);
    if(response.code == 200) {
        console.log(response);
        goto(UI_URL.passwordUpdated);
    }
    if(response.code == 400) {
        $("#confirm_password_check").html(response.message).css("color", "red");
    }
}

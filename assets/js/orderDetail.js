window.addEventListener('DOMContentLoaded', function (event) {
    if (localStorage.getItem(STORAGE.accesstoken) && localStorage.getItem(STORAGE.userId)) {
        const params = new URLSearchParams(window.location.search)
        let order_id = params.get('oid')
        if(order_id) {
            orderDetail(order_id);
        } else {
            goto(UI_URL.buyerOrders);
        }
    }
});

function orderDetail(order_id) {
    __ajax_http("buyer/order/details/"+order_id, "", headers(), AJAX_CONF.apiType.GET,"", __success_orderDetail);
}

function __success_orderDetail(response) {
    console.log(response);
    let message = window.localStorage.getItem("message");
    if(message) {
        $(".alert-success").show().html(message);
        setTimeout(function() {
            $(".alert-success").hide();
            window.localStorage.removeItem("message");
        }, 4000);
    }
    let detailHtml = '';
    let total = response.orderDetails.order_amount;
    let orderId = response.orderDetails.id;
    let date  = response.orderDetails.created_at.split('T')[0];
        date = new Date(date);
        date = date.toDateString().split(' ').slice(1).join(' ');
    // let btn = "shipped-btn";
    // let order_status = response.message;
    if(response.orderDetails.order_status == 0) {
        btn = "shipped-btn";
        order_status = "Order Placed";
    }
    if(response.orderDetails.order_status == 1) {
        btn = "shipped-btn";
        order_status = "Order Received";
    }
    if(response.orderDetails.order_status == 2) {
        btn = "dispatch-btn";
        order_status = "Dispatched";
    }
    if(response.orderDetails.order_status == 3) {
        btn = "delivered-btn";
        order_status = "Delivered";
    }
    response.data.forEach(element => {
        orderId = element.order_id;
        // total += element.amount;
        date  = element.created_at.split('T')[0];
        date = new Date(date);
        date = date.toDateString().split(' ').slice(1).join(' ');
        
        detailHtml += '<div class="product-box-content"  style="margin-bottom: 20px;">'
        detailHtml +=   '<div class="product-details">'
        detailHtml +=         '<img src="'+ MEDIA_URL + element.product_image +'">'
        detailHtml +=         '<div class="box-content">'
        detailHtml +=             '<p>'+ element.product_title +'</p>'
        detailHtml +=             '<h2>KSH '+ element.amount +'</h2>'
        detailHtml +=             '<p>Qty '+ element.quantity +'</p>'
        detailHtml +=         '</div>'
        detailHtml +=     '</div>'
        if(response.orderDetails.order_status == 3) {
            
            if(element.ratings_and_reviews == null) {
                detailHtml +=     '<div class="p-rating">'
                detailHtml +=         '<div class="rating">'
                for(let j=0; j< 5; j++) {
                    detailHtml+=                   '<i class="fa fa-star-o" style="font-size: 27px;margin-top: 4px;"></i>'
                }
                detailHtml +=         '</div">'
                detailHtml +=         '</div>'
                detailHtml +=         '<div class="review" onclick="reviewPage('+ element.business_product_id +',\''+ element.product_image + '\',\'' + element.product_title +'\','+ element.order_id +')">'
                detailHtml +=             'Write Review'
                detailHtml +=         '</div>'
            } else {
                detailHtml +=     '<div class="p-rating">'
                detailHtml +=         '<div class="rating">'
                let gold = element.ratings_and_reviews.rating;
                let empty = 5 - element.ratings_and_reviews.rating;
                for(let i=0; i< gold; i++) {
                    detailHtml+=                   '<i class="fa fa-star" style="font-size: 27px;margin-top: 4px;"></i>'
                }
                for(let j=0; j< empty; j++) {
                    detailHtml+=                   '<i class="fa fa-star-o" style="font-size: 27px;margin-top: 4px;"></i>'
                }
                detailHtml +=         '</div">'
                detailHtml +=         '</div>'
                detailHtml +=         '<div class="review" onclick="seeReview('+ element.business_product_id +',\''+ element.product_image + '\',\'' + element.product_title +'\','+ element.order_id +')">'
                detailHtml +=             'See Your Review'
                detailHtml +=         '</div>'
            }
        }
        
        detailHtml +=     '</div>'
        detailHtml +=  '</div>'

    });
    $(".orderBtn").addClass(btn);
    $("#orderStatus").html(order_status);
    $(".order-box-content").html(detailHtml);
    $("#orderId").html(orderId);
    $("#orderAmount").html(total);
    $("#orderDate").html(date);
    console.log(order_status);
}

function reviewPage(product_id, product_image, product_title, order_id) {
    setLocalStorage('image', product_image);
    setLocalStorage('title', product_title);
    setLocalStorage('oid', order_id);
    location.href = 'buyer-review-product.php?rid='+product_id;
}

function seeReview(product_id, product_image, product_title, order_id) {
    setLocalStorage('image', product_image);
    setLocalStorage('title', product_title);
    setLocalStorage('oid', order_id);
    location.href = 'buyer-edit-review.php?rid='+product_id;
}

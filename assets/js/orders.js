window.addEventListener('DOMContentLoaded', function (event) {
    if (localStorage.getItem(STORAGE.accesstoken) && localStorage.getItem(STORAGE.userId)) {
        myOrders();
    }
    updateCartIcon();

});

function myOrders() {
    __ajax_http("buyer/order/listing", "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __orderListing);
}

function __orderListing(response) {
    console.log(response);
    let html = '';
    if(response.data.length == 0) {
        console.log("No Orders");
        html += '<div class="empty-wrapper w-100">'
        html +=   '<div class="text-center w-100 ">'
        html +=       '<img src="images/icons/empty.svg" alt="">'
        html +=    '</div>'
        html +=    '<p class="text-center">Oops No Orders Found. :(</p>'
        html +=    '<div class="btn-wrapper text-center mt-3">'
        // html +=        '<a href="#" class="white-bttn p-2 disply-inline-block">Go to product page</a>'
        html +=    '</div>'
        html += '</div>'
    }
    response.data.forEach(element => {
        let btn;
        let order_status;
        if(element.order_status == 0) {
            btn = "shipped-btn";
            order_status = "Order Placed";
        }
        if(element.order_status == 1) {
            btn = "shipped-btn";
            order_status = "Order Received";
        }
        if(element.order_status == 2) {
            btn = "dispatch-btn";
            order_status = "Dispatched";
        }
        if(element.order_status == 3) {
            btn = "delivered-btn";
            order_status = "Delivered";
        }
        if(element.payment_status==1){
        html += '<div class="box-container col-12">'
        html += '<ul class="order-id">'
        html +=    '<li>Order ID: '+ element.id +'</li>'
        html +=   ' <li><img src="images/icons/chevron-right.svg" alt="" onclick="orderDetail('+ element.id +')"></li>'
        html += '</ul>'
        html += '<h3>Order Amount: KSH <span class="order-amount">'+ element.order_amount +'</span></h3>'
        html += '<ul class="order-date">'
        let date = element.created_at.split('T')[0];
        date = new Date(date);
        html +=   ' <li>Date: '+ date.toDateString().split(' ').slice(1).join(' '); +'</li>'
        html +=    '<li><button class="'+ btn +'">'+ order_status +'</button></li>'
        html += '</ul>'
        html += '</div>'
        }
    });
    $(".order-box").html(html);
    
}


function orderDetail(order_id) {
    // goto(UI_URL.orderDetail);
    location.href = 'buyer-order-detail.php?oid='+order_id;

}
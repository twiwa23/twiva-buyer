window.addEventListener('DOMContentLoaded', function (event) {
    if (!user_id && !accessToken) {
        goto(UI_URL.buyerLogin);
    }
    if(location.pathname == '/buyer-place-order.php') {
        cartCountUpdate(res => {
            if(res.data.length == 0)
                goto(UI_URL.buyerCart);
        });
    }
    let error = window.localStorage.getItem("error");
    if(error) {
        $(".order-fail-message").html(error);
    }
    phoneCodes();
    
});

function phoneCodes() {
    __ajax_http("influencer/country-code/list", "", headers(), AJAX_CONF.apiType.GET,"", __success_phone_codes);
}

function __success_phone_codes(response) {
    console.log(response);
    if(response.status === true) {
        response.data.forEach(element => {
            $('#phone-codes').append(`
                <option value="${element.phone_code}">${element.country_code}  +${element.phone_code}</option>`
            ) 
        });
    } else {
        $('#phone-codes').append(`
                <option value="">phone Not Found</option>`
        )
    }
}

function payNow() {
    var address_id = window.localStorage.getItem('address_id');
    let phone_number = $("#phone_number").val();
    phone_number = phone_number.replace(/\s+/g, '');
    if(phone_number == '') {
        $("#error").html("Please enter the Mobile Number");
        return;
    }
    if(address_id == '' || address_id === null) {
        $("#error").html("Please Select the address");
        return;
    }
    $("#payNowBtn").attr("disabled", true);
    $("#payNowBtn").html("Please wait....");
    let phone_code = $("#phone-codes").val();
    let valid_phone_number = phone_code + phone_number;
    // console.log(valid_phone_number);
    var data = {
        "phone_number": valid_phone_number,
        "address_id": address_id
    }
    console.log(data);
    try {
        payMPesa(data);
    } catch(err) {
        console.log(err);
    }

}

function payMPesa(data) {
    __ajax_http("buyer/order/place-order", data, headers(), AJAX_CONF.apiType.POST,"", __success_payMPesa);
}
var mPesaRequest='';
function __success_payMPesa(response) {
    console.log(response.error);
    if(response.status === true) {
        removeLocalStorage("total");
        removeLocalStorage("address_id");
	    var mPesaRequest={"merchant_id":response.tansaction_details.MerchantRequestID};
		    __ajax_http("buyer/check-payment-status", mPesaRequest, headers(), AJAX_CONF.apiType.POST,"", __success_response);	    
    } 
    if(response.status === false) {
        $("#error").html(response.error);
        setLocalStorage("error", response.message);
        goto(UI_URL.orderFailure);
    } 
}

function __success_response(response){
    var mPesaRequest={"merchant_id":response.merchant_id};
    if(response.transaction_status == 0) {
        __ajax_http("buyer/check-payment-status", mPesaRequest, headers(), AJAX_CONF.apiType.POST,"", __success_response);
     }if(response.transaction_status == 1) {
        goto(UI_URL.orderSuccess);
     }else{
        $("#error").html(response.error);
        setLocalStorage("error", response.message);
        goto(UI_URL.orderFailure);
     }	
}
	

window.addEventListener('DOMContentLoaded', function (event) {
    if (localStorage.getItem(STORAGE.accesstoken) && localStorage.getItem(STORAGE.userId)) {
        const params = new URLSearchParams(window.location.search)
        let product_id = params.get('rid')
        if(product_id) {
            review(product_id);
        } else {
            localStorage.removeItem('title');
        }
    }
});




function review(product_id) {
    $(".review-product-title").html(window.localStorage.getItem("title"))
    $(".review-product-image").attr('src', MEDIA_URL + window.localStorage.getItem("image"))
}

function submitReview(){
    const params = new URLSearchParams(window.location.search)
    let product_id = params.get('rid')
    let data = {
        "product_id": product_id,
        "rating": $('input[name="stars"]:checked').val(),
        "review":$("#review").val()
    }
    console.log(data);
    addReview(data);
}

function addReview(data) {
    __ajax_http("buyer/reviews/add", data, headers(), AJAX_CONF.apiType.POST,"", __success_add_review);
}

function __success_add_review(response) {
    console.log(response);
    if(response.status == true) {
        let order_id = window.localStorage.getItem("oid");
        window.localStorage.setItem("message", "Thanks For Giving your Review");
        if(order_id){
            location.href = 'buyer-order-detail.php?oid='+order_id;
        }
        else{
            window.history.back();
        }
    }
    if(response.status == false) {
        window.localStorage.setItem("message", response.error);
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 4000);
    }
}

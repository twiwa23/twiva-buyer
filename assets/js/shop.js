//
//#Shop js
//\
window.addEventListener('DOMContentLoaded', function (event) {

getShopDetails(405,1,5)

});

function getShopDetails(shopId,page,limit) {
    __ajax_http("buyer/shop/products/"+ shopId+"?page="+page+"&limit="+limit, "", headers(), AJAX_CONF.apiType.GET, "GET Sponsee", __success_shop_listing);
}

function __success_shop_listing(response)
{
    $(".banner-image").attr("src", MEDIA_URL + response.shop_details.shop_cover_image_path);
    $("#shop-logo").attr("src", MEDIA_URL + response.shop_details.shop_logo_path);
    $(".shop-description").text(response.shop_details.shop_description);
    $("#shop-name").text(response.shop_details.shop_name);

    appendHTML="";

    var id=0;
    var inf_id=0;
    response.shop_product_details.data.forEach(function(element) {
        
        id=btoa(btoa(element.product_id));
        inf_id = btoa(btoa(element.influencer_id));
        appendHTML += '<div class="product-box-inner col-6 col-md-4 col-lg-3">' +
        '  <a href="/buyer-product-detail.php?in='+inf_id+'&pid='+id+'">'+
            '<div class="product-box-content">';
            element.influencer_product_images.forEach(function(el) {
                if(el.is_cover_pic==1){
                    appendHTML += ' <img height="100px" src="'+MEDIA_URL+el.image_path+'" />';
                }
            });
            
            appendHTML += '<div class="box-content">' +
            '    <p>'+element.product_title+'</p>' +
            ' <h2>'+"KSH" +" "+element.product_price+'</h2>' +        
               
            '</div>' +
            '<div class="product-rating">' +
            '<span class="rating-title">5.0</span>' +
            ' <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>' +
            '</div>' +
             '</a>' + 
            '</div>' +
            '</div>';
        // }
    });
    appendHTML += '</div>';
$("#product-list").html(appendHTML);

paginationHTML='';

paginationHTML+='<li class="page-item">'+
'<a class="page-link left-arrow" href="javascript:getShopDetails(405,'+response.shop_product_details.current_page  +',5); ><img src="'+ IMAGES_PATH+'/icons/Left.svg" alt="" /></a></li>';

for(i=1;i<=response.shop_product_details.last_page;i++){
    if(i==response.shop_product_details.current_page){
        paginationHTML+='<li class="page-item"><a class="page-link page-link-active" href="javascript:getShopDetails(405,'+i +',5);">'+i+'</a></li>';
    }
    else{
        paginationHTML+='<li class="page-item"><a class="page-link" href="javascript:getShopDetails(405,'+i +',5);">'+i+'</a></li>';
    }
    
}
paginationHTML+='<li class="page-item">'+
'<a class="page-link right-arrow" href="javascript:getShopDetails(405,'+response.shop_product_details.current_page +',5);><img src="'+IMAGES_PATH+'/icons/Right.svg" alt="" /></a></li>';

$("#pagination").html(paginationHTML);


}

window.addEventListener('DOMContentLoaded', function (event) {
    var sidemenuHtml = '';
    if (localStorage.getItem(STORAGE.accesstoken) && localStorage.getItem(STORAGE.userId)) {
        sidemenuHtml += '<li>'
        sidemenuHtml +=     '<a href="/buyer-orders.php"><img src="images/icons/order.svg" />My Orders<span class="fa fa-chevron-down"></span></a>'
        sidemenuHtml +=  '</li>'
        sidemenuHtml += '<li>'
        sidemenuHtml +=     '<a href="/buyer-account-settings.php"><img src="images/icons/settings.svg" />Account Settings<span class="fa fa-chevron-down"></span></a>'
        sidemenuHtml +=  '</li>'
        sidemenuHtml += '<li>'
        sidemenuHtml +=     '<a href="/buyer-support.php"><img src="images/icons/Vector-support.svg" />Support<span class="fa fa-chevron-down"></span></a>'
        sidemenuHtml +=  '</li>'
        sidemenuHtml += '<li>'
        sidemenuHtml +=     '<a onclick="logoutBuyer()"><img src="images/icons/log-out.svg" />Logout<span class="fa fa-chevron-down"></span></a>'
        sidemenuHtml +=  '</li>'
        
    } else {
        sidemenuHtml += '<li>'
        sidemenuHtml +=     '<a href="/buyer-login.php"><img src="images/icons/log-out.svg" />Login<span class="fa fa-chevron-down"></span></a>'
        sidemenuHtml +=  '</li>'
        
    }

    $(".side-menu").append(sidemenuHtml);
});
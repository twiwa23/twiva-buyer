window.addEventListener('DOMContentLoaded', function (event) {
    reasons();
});

function reasons() {
    __ajax_http("buyer/reasons/list", "", headers(), AJAX_CONF.apiType.GET, "GET Reasons", __success_reasons);
}

function __success_reasons(response) {
    console.log(response);
    if(response.status == true) {
        var reasonHtml = '';
        response.data.forEach(element => {
            $('#reason_id').append(`
                <option value="${element.id}">${element.reason}</option>`
            ) 
        });
        
    }
    $(".reason-list").append(reasonHtml);
}

function addSupport() {
    var data = {
        "reason_id" : $("#reason_id").val(),
        "description" : $("#description").val()
    }
    updateSupport(data);
}

function updateSupport(data) {
    __ajax_http("buyer/reasons/contact-support", data, headers(), AJAX_CONF.apiType.POST, "Post Reasons", __success_update_support);
}

function __success_update_support(response) {
    console.log(response);
    if(response.status == true) {
        $(".alert-success").show().html("We'll get Back to you shortly");
        
        setTimeout(function() {
            $(".alert-success").hide();
            location.reload();
        }, 5000);
    }
    if(response.status == false) {
        $(".alert-danger").show().html(response.error);
        setTimeout(function() {
            $(".alert-danger").hide();
        }, 7000);
    }
}
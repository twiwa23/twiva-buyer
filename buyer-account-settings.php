<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>


    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>
<div class="login" id="account-settings">

        <div class="back-link" >
            <a href="#">Account Settings</a>
        </div>
        <div class="alert alert-success review-alert" role="alert" style="display: none; position: fixed; z-index:999" ></div>

        <div class="account-connections">
            <ul class="account-list">
                <!-- <li class="account-list-item">
                    <a href="">Payment Account</a>
                    <img src="./images/icons/chevron-right.svg" alt="">
                </li> -->
                <li class="account-list-item">
                    <a href="/buyer-manage-address.php">Manage Address</a>
                    <img src="./images/icons/chevron-right.svg" alt="">
                </li>
                <li class="account-list-item">
                    <a href="/buyer-change-password.php">Change Password</a>
                    <img src="./images/icons/chevron-right.svg" alt="">
                </li>
                
            </ul>
        </div>

            <script src="assets/js/api.js"></script>
            <script src="assets/js/cart.js"></script>
            <script src="assets/js/login.js"></script>
            <script src="assets/js/detail.js"></script>
            
            <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
            <script>
                let message = window.localStorage.getItem("message");
                if(message) {
                    $(".alert-success").show().html(message);
                    setTimeout(function() {
                        $(".alert-success").hide();
                        window.localStorage.removeItem("message");
                    }, 4000);
                }
            </script>
            <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
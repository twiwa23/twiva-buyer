<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page" role="main" id="cart-page">
                <div class="page-title">
                    <a href="javascript:history.go(-1);">
                        <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""></span>
                        My Cart
                    </a>
                    <div onclick="clearCart()" class="cart-clear" style="float:right">Clear Cart</div>
                </div>
                <div class="dashboard-inner">
                    <div class="product-section">
                        <div class="product-box row cart-box">


							<!-- <div class="product-box-inner col-12 col-sm-6 col-md-4">
									<div class="product-box-content">
									
										<img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
									
										
										<div class="box-content">
											<p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
											<h2>KSH 22.99</h2>
											<div class="dropdown-content">
												<div class="form-field">
													<div class="dropdown multi-select-options d-flex product-size">
													
													<select>
														<option value="">Size</option>
															<option value="">S</option>
															<option value="">L</option>
															<option value="">M</option>
															<option value="">XS</option>
														</select>
													</div>
												</div>
												<div class="form-field">
													<div class="dropdown multi-select-options d-flex product-color">
													
													<select>
														<option value="">Color</option>
															<option> Red </option>
															<option> Green </option>
															<option> Blue </option>
														</select>
													</div>
												</div>
											</div>
											<div class="quantity">
													<button class="pluse" onclick="decreaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')">-</button>
													<input type="text"  class="total-quantity"  id="quantity-text'+parsedCart[i]["product_id"]+'" value="'+ parsedCart[i]["quantity"] +'" disabled size="4" >
													<button class="min" onclick="increaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')"> +</button>
												<div id="stock-error'+parsedCart[i]["product_id"]+'" style="color:red"></div>
											</div>
											<span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Delete.svg" alt="">REMOVE</span>
										</div>
										<div class="product-rating">
											<span class="rating-title">5.0</span>
											<span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
										</div>
							</div>
								</div>  -->
							<!-- <div class="product-box-inner col-12 col-sm-6 col-md-4">
								<div class="product-box-content">

									<img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">


									<div class="box-content">
										<p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
										<h2>KSH 22.99</h2>
										<div class="dropdown-content">
											<div class="form-field">
												<div class="dropdown multi-select-options d-flex product-size">

												<select>
													<option value="">Size</option>
														<option value="">S</option>
														<option value="">L</option>
														<option value="">M</option>
														<option value="">XS</option>
													</select>
												</div>
											</div>
											<div class="form-field">
												<div class="dropdown multi-select-options d-flex product-color">

												<select>
													<option value="">Color</option>
														<option> Red </option>
														<option> Green </option>
														<option> Blue </option>
													</select>
												</div>
											</div>
										</div>
										<div class="quantity">
												<button class="pluse" onclick="decreaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')">-</button>
												<input type="text"  class="total-quantity"  id="quantity-text'+parsedCart[i]["product_id"]+'" value="'+ parsedCart[i]["quantity"] +'" disabled size="4" >
												<button class="min" onclick="increaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')"> +</button>
											<div id="stock-error'+parsedCart[i]["product_id"]+'" style="color:red"></div>
										</div>
										<span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Delete.svg" alt="">REMOVE</span>
									</div>
									<div class="product-rating">
										<span class="rating-title">5.0</span>
										<span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
									</div>
								</div>
							</div> -->

							<!-- <div class="product-box-inner col-12 col-sm-6 col-md-4">
								<div class="product-box-content">

									<img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">


									<div class="box-content">
										<p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
										<h2>KSH 22.99</h2>
										<div class="dropdown-content">
											<div class="form-field">
												<div class="dropdown multi-select-options d-flex product-size">

												<select>
													<option value="">Size</option>
														<option value="">S</option>
														<option value="">L</option>
														<option value="">M</option>
														<option value="">XS</option>
													</select>
												</div>
											</div>
											<div class="form-field">
												<div class="dropdown multi-select-options d-flex product-color">

												<select>
													<option value="">Color</option>
														<option> Red </option>
														<option> Green </option>
														<option> Blue </option>
													</select>
												</div>
											</div>
										</div>
										<div class="quantity">
												<button class="pluse" onclick="decreaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')">-</button>
												<input type="text"  class="total-quantity"  id="quantity-text'+parsedCart[i]["product_id"]+'" value="'+ parsedCart[i]["quantity"] +'" disabled size="4" >
												<button class="min" onclick="increaseQuantity('+ parsedCart[i]["product_id"] +','+ product_detail.stock+','+ product_detail.price+')"> +</button>
											<div id="stock-error'+parsedCart[i]["product_id"]+'" style="color:red"></div>
										</div>
										<span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Delete.svg" alt="">REMOVE</span>
									</div>
									<div class="product-rating">
										<span class="rating-title">5.0</span>
										<span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
									</div>
								</div>
							</div> -->


                              
                        </div>
						<!-- empty state start-->
						<div class="empty_cart">
							</div>
                             <!-- empty state start-->    
                    </div>
                </div>

            <div class="checkout-section">
                <h3>Order Summary</h3>
                <ul>
                    <li>Total:</li>
                    <li>KSH <span id="total"></span></li>
                </ul>
                <button class="purple-btn" id="checkout-btn" onclick="checkLoginStatus()">Proceed to Checkout</button>
            </div>

        </div>
    </div>

<script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/login.js" ></script>
<script  src="assets/js/detail.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

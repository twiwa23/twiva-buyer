<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>


  <!--Main Section Start-->
  <div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
  </div>




    <div class="login" id="change-password">

        <div class="back-link">
            <a href="javascript:history.go(-1);"> <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""> Change Password</a>
        </div>

        <div class="container">
            <div class="login-inner">

                <div class="login-left">
                    <!-- <img src=".<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->

                </div>

                <div class="login-right">

                    <div class="login-section">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>

                        <p>Please enter a new password and confirm it to reset.</p>
                        <div class="alert alert-success" role="alert" style="display: none; position: fixed"></div>
                        <div class="alert alert-danger" role="alert"  style="display: none; position: fixed"></div>
                        <h5 id="password_check"></h5>
                                <div class="input-field">
                                    <label>Current Password</label>
                                    <input id="current_password" type="password" placeholder="Current Password" onkeydown="checkCurrentPasswordLength()">
                                    <span id="current_password_check"></span>
                                </div>

                                <div class="input-field">
                                    <label>New Password</label>
                                    <input id="new_password" name="new_password" type="password" placeholder="Enter New Password" onkeyup="checkPasswordLength()">
                                    <span id="new_password_check"></span>
                                </div>

                                <div class="input-field">
                                    <label>Re-enter Password</label>
                                    <input id="confirm_password" name="confirm_password" type="password" placeholder="Confirm Password" onkeyup="checkPasswordMatch()">
                                    <span id="confirm_password_check"></span>
                                </div>

                                <button onclick="validatePassword();" id="updatePassword">Update</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>
    </div>


<script src="assets/js/api.js"></script>
<script src="assets/js/cart.js"></script>
<script src="assets/js/change-password.js"></script>

        <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
        <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

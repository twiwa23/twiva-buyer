<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="login" id="change-password">

        <div class="back-link" style="padding-top:64px;">
            <a href="#"> <img src="./images/icons/chevron-left.svg" alt=""> Forgot Password</a>
        </div>

        <div class="container">
            <div class="login-inner">
        
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right">
                
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>
                
                        <p>Please enter your registered email address. We will send you a link to reset your password.</p>

                        <h5 id="passwordcheck" style="display: none;"></h5>

                                <div class="input-field">
                                    <label>E-mail Address</label>
                                    <input id="forgot_password_mail_id" type="email" placeholder="Enter E-mail Address" onkeyup="validateEmailID()">
                                </div>
                                <span id="email_check"></span>

                               
                                
                        <button id="send_link" onclick="forgotPassword()">Send</button>
                    </div>
                </div> 
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>
    </div>




<script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/login.js" ></script>
<script  src="assets/js/detail.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

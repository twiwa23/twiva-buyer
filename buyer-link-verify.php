<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="back-link" id="account-confirm-header">
        <a href="#"> <i class='fas fa-chevron-left'></i></a>
    </div>

    <div class="account-confirm">
        <img src="./images/icons/Frame 7032.svg" alt="">
        <h3>Check your Email</h3>
        <p>We've sent an OTP to reset the password</p>
        <a href="/buyer-reset-otp-verify.php" class="mt-2 d-inline-block white-bttn c-btn ">Enter OTP</a>

    </div>




<script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/login.js" ></script>
<script  src="assets/js/detail.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>
    <div class="login" id="buyer-login">

        <div class="back-link">
            <a href="#">Cart <img src="./images/icons/chevron-right.svg" alt=""></a>
            <a href="#">Information <img src="./images/icons/chevron-right.svg" alt=""></a>
            <a href="#">Shipping <img src="./images/icons/chevron-right.svg" alt=""></a>
            <a href="#">Payment</a>
        </div>
        
        <div class="container">

            <div class="order-summary">
                <ul>
                    <li><img src="./images/icons/cart-outline.svg" alt=""></li>
                    <li class="text">Order Summary</li>
                    <!-- <li><img src="./images/icons/arrow-down_black.svg" alt=""></li> -->
                </ul>
                <h3>KSH  <span id="total">200.00</span></h3>
            </div>


            <div class="login-inner">

            
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right">
                
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>

                        <h2 class="login-header">
                            Login to your account
                        </h2>

                        <div class="input-field">
                            <label>E-mail Address</label>
                            <input type="text" placeholder="email" name="email" id="email">
                        </div>
                    
                        <div class="input-field">
                            <label>Password</label>
                            <input type="password" placeholder="*******" name="password" id="password">
                        </div>
                        <div id="emailError" style="display: none;"></div>
                        <div class="forgot-pass"><a href="/buyer-forgot-password.php">Forgot Password?</a>
                        <a id="verify_email_button" onclick="reVerify()" style="display: none;">Click here to Verify</a>
                    
                    </div>
                
                        <button onclick="login()">Login</button>
                        <div class="no-account">
                            Don’t have an account? <a href="/buyer-signup.php">Sign Up</a>
                        </div>
                    </div>
                </div> 
            
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>
    </div>

    
    

        <script  src="assets/js/api.js" ></script>
        <script  src="assets/js/login.js" ></script>
        <script  src="assets/js/cart.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
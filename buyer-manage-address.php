<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="" id="buyer-orders">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page manage-address" role="main">
                <div class="page-title">
                    <a href="javascript:history.go(-1);"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="">Manage Address</a>
                </div>
                <div class="page-heading">
                    <a href="/buyer-add-address.php"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/plus.svg" alt="">ADD NEW ADDRESS</a>
                </div>
                <div class="alert alert-success" role="alert" style="display: none; position: fixed; z-index:999"></div>

                <div class="dashboard-inner">
                    <div class="product-section">
                        <div class="product-box row address-box">


                        </div>
                    </div>
                </div>

            </div>
    </div>

<script  src="assets/js/api.js"></script>
<script  src="assets/js/address.js"></script>
<script  src="assets/js/payment.js"></script>
<script  src="assets/js/cart.js"></script>

<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

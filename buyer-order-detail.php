<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="" id="buyer-orders">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page order-detail" role="main">
                <div class="page-title">
                    <a href="javascript:history.go(-1);"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="">Order Detail</a>
                </div>
                <div class="alert alert-success review-alert" role="alert" style="display: none; position: fixed; z-index:999" ></div>

                <div class="dashboard-inner">
                    <div class="product-section">
                        <div class="product-box">

                            <div class="box-container col-12">
                                <ul class="order-id">
                                    <li>Order ID: <span id="orderId"></span></li>
                                    <!-- <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></li> -->
                                </ul>
                                <h3>Order Amount : KSH <span id="orderAmount" class="order-amount"></span></h3>
                                <ul class="order-date">
                                    <li>Date: <span id="orderDate"></span></li>
                                    <li><button class="btn orderBtn" style="color:white"><span id="orderStatus"></span></button></li>
                                </ul>
                            </div>

                            <div class="product-box-inner order-box-content">
                                <!-- <div class="product-box-content"> -->
                                    <!-- <div class="product-details">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame_1971.png">
                                        <div class="box-content">
                                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                            <h2>$22.99</h2>
                                        </div>
                                    </div>

                                    <div class="p-rating">
                                        <div class="rating">
                                            <label>
                                              <input type="radio" name="stars" value="1" />
                                              <span class="icon">★</span>
                                            </label>
                                            <label>
                                              <input type="radio" name="stars" value="2" />
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                            </label>
                                            <label>
                                              <input type="radio" name="stars" value="3" />
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                            </label>
                                            <label>
                                              <input type="radio" name="stars" value="4" />
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                            </label>
                                            <label>
                                              <input type="radio" name="stars" value="5" />
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                              <span class="icon">★</span>
                                            </label>
                                        </div>

                                        <div class="review">
                                            Write Review
                                        </div>
                                    </div> -->
                                <!-- </div> -->
                            </div>

                        </div>
                    </div>
                </div>

            </div>
    </div>

    <script  src="assets/js/api.js"></script>
    <script  src="assets/js/login.js"></script>
    <!-- <script  src="assets/js/cart.js" ></script> -->
    <!-- <script  src="assets/js/detail.js" ></script> -->
    <!-- <script  src="assets/js/address.js" ></script> -->
    <!-- <script  src="assets/js/payment.js" ></script> -->
    <script  src="assets/js/orders.js" ></script>
    <script  src="assets/js/orderDetail.js" ></script>


<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

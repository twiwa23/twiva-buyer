<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="container-fluid container-padding">

    <div class="row confirm-order-main-wrapper">
        <div class="col-md-12 confirm-address-wrapper">
            <div class="text-center mt-4 mb-5">
                <img width="250px" class="payment-img" src="images/icons/success.svg" alt="">
                <h5 class="text-center mt-4 mb-2 ">Payment Successful</h5>
                <a href="/buyer-orders.php" class="mt-2 d-inline-block white-bttn c-btn ">Go To Orders</a>
            </div>
        </div>


    </div>

</div>


<script src="assets/js/api.js"></script>
<script src="assets/js/login.js"></script>
<script src="assets/js/cart.js"></script>
<!-- <script  src="assets/js/detail.js" ></script> -->
<!-- <script src="assets/js/address.js"></script> -->

<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
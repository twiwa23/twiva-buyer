<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="" id="buyer-orders">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page" role="main">
                <div class="page-title">
                    <a href="">My Orders</a>
                </div>
                <div class="dashboard-inner">
                    <div class="product-section">
                        <div class="product-box order-box row">

                            <!-- <div class="box-container col-12">
                                <ul class="order-id">
                                    <li>Order ID: 293857</li>
                                    <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></li>
                                </ul>
                                <h3>Order Amount: $50.00</h3>
                                <ul class="order-date">
                                    <li>Date: 18/05/2021</li>
                                    <li><button class="dispatch-btn">Dispatched</button></li>
                                </ul>
                            </div> -->

                        </div>
                    </div>
                </div>

            </div>
    </div>

    <script  src="assets/js/api.js"></script>
    <script  src="assets/js/login.js"></script>
    <!-- <script  src="assets/js/cart.js" ></script> -->
    <!-- <script  src="assets/js/detail.js" ></script> -->
    <script  src="assets/js/address.js" ></script>
    <script  src="assets/js/payment.js" ></script>
    <script  src="assets/js/orders.js" ></script>


<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

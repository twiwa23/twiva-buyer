<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="login">

     
        
<div class="container">
    <div class="login-inner">
      
    <div class="login-left">
        <!-- <img src="../images/banner/login.png"> -->
       
    </div>

    <div class="login-right">
       
        <div class="login-section">
            <!-- <div class="logo"><img src="images/logo/logo.svg"></div> -->
              <h5 style="display: none;" class="login-error alert alert-success" role="alert" id="otpsuc" ></h5>
    
            <p>Enter the code sent your email</p>
            <div class="input-field">
                <label>Enter Code</label>
                <input id="otp" type="text" required>
                <span id="otp-error"></span>
            </div>

            

            
            <button onclick="verifyOtp()">Confirm</button>
            <div class="no-account">
                Back to <a href="buyer-login.php">Login</a>
            </div>
        </div>
    </div> 
    
</div>
</div>
<!-- <div class="container">
    <div class="login-inner">
      
    <div class="login-left"> -->
        <!-- <img src="../images/banner/login.png"> -->
       
    <!-- </div>

    <div class="login-right">
       
        <div class="login-section">
            <div class="logo"><img src="images/logo/logo.svg"></div>
              <h5 style="display: none;" class="login-error alert alert-success" role="alert" id="otpsuc" ></h5>
    
            <p>Enter your OTP to verify</p>
            <div class="input-field">
                <label>OTP</label>
                <input id="otp" type="text">
            </div>

            

            
            <button onclick="verifyOtp()">Confirm</button>
            <div class="no-account">
                Back to <a href="buyer-login.php">Login</a>
            </div>
        </div>
    </div> 
    
</div>
</div> -->

<script  src="assets/js/login.js" ></script>
<script  src="assets/js/api.js" ></script>
<script  src="assets/js/cart.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
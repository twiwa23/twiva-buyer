<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="account-setup">
    <div class="container">

        <div class="welcome-screen">
            <img src="./images/icons/welcome-icon.png">
            <h3>Password Updated</h3>
            <p>Please login to continue</p>

            <div class="button-sec">
                <button><a href="/buyer-login.php">Login</a></button>
            </div>
        </div>


    </div>
</div>


<script src="assets/js/api.js"></script>
<script src="assets/js/login.js"></script>

<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
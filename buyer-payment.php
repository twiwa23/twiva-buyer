<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
              <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>


    <div class="login" id="buyer-login">

        <div class="back-link">
            <a href="#">Cart <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Information <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Shipping <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Payment</a>
        </div>

        <div class="container" id="buyer-shipping">

            <div class="order-summary">
                <ul>
                    <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/cart-outline.svg" alt=""></li>
                    <li class="text">Order Summary</li>
                    <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt=""></li>
                </ul>
                <h3>$200.00</h3>
            </div>


            <div class="login-inner" id="buyer-payment">

                <div class="login-left">
                    <!-- <img src=".<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->

                </div>

                <div class="login-right">

                    <div class="login-section">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>

                        <div class="login-header">
                            How would you like to pay?
                        </div>

                        <div class="form-input-field">
                            <div class="radio-input">
                                <div>
                                    <input type="radio"  name="payment">
                                    <label>Pay Now</label>
                                </div>
                                <div>
                                    <input type="radio"  name="payment">
                                    <label>Pay Later</label>
                                </div>
                            </div>
                        </div>

                        <div class="container">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <label>Method</label>
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseRegion">
                                                Select Option
                                                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt="">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseRegion" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <ul>
                                                <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                    <label class="form-check-label" for="defaultCheck1">
                                                        Credit Card
                                                    </label></li>
                                                <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                    <label class="form-check-label" for="defaultCheck1">
                                                        Debit Card
                                                    </label></li>
                                                <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                    <label class="form-check-label" for="defaultCheck1">
                                                        Net Banking
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="input-field">
                            <label>Card Number</label>
                            <input type="text" placeholder="XXXX - XXXX - XXXX - XXXX">
                        </div>

                        <div class="input-field card-details">
                            <div class="card-content" style="margin-right: 16px;">
                                <label>CVV</label>
                                <input type="text" placeholder="Enter CVV">
                            </div>
                            <div class="card-content">
                                <label>Expiration</label>
                                <input type="month" placeholder="MM/YY">
                            </div>
                        </div>


                        <button>Place Order</button>


                    </div>
                </div>

            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>

    </div>

        <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
        <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>


    <div class="login" id="buyer-login">

        <div class="back-link">
            <a href="#">Cart <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Information <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Shipping <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Payment</a>
        </div>

        <div class="container" id="buyer-shipping">

            <div class="order-summary">
                <ul>
                    <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/cart-outline.svg" alt=""></li>
                    <li class="text">Order Summary</li>
                    <!-- <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt=""></li> -->
                </ul>
                <h3>KSH  <span id="total"></span></h3>
            </div>


            <div class="login-inner" id="buyer-payment">
            
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right">
                
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>

                        <div class="login-header">
                            How would you like to pay?
                        </div>

                        <div class="form-input-field">
                            <div class="radio-input">
                                <div>
                                    <input type="radio" name="payment" id="pay_mpesa">
                                    <label>Pay through MPesa</label>
                                </div>
                                <!-- <div>
                                    <input type="radio"  name="payment">
                                    <label>Pay Later</label>
                                </div> -->
                            </div>
                        </div>

                        <div class="">

                        <div class="input-field">
                            <label>Phone Number</label>
                            <div class="phone-no-wrapper">
                            <select id="phone-codes" class="form-control">
                                <option value="254">KE +254</option>
                            </select>
                            <input type="number" placeholder="Enter Phone Number" id="phone_number" max="10" required>
                        
                            </div>
                            </div>
                        </div>
                        <div id="error" style="color: red;"></div>

                        <!-- <div class="input-field">
                            <p>Pay later solution which lets you shop now and pay later anytime within 14 days. </p>
                        </div> -->

                        <!-- <div class="input-field" id="upload-section">
                            <label>Upload Document</label>
                            <input type="file" placeholder="Upload Image">
                        </div> -->
                    
                        
                        
                
                        <button onclick="payNow()" id="payNowBtn">Pay Now</button>

                        
                    </div>
                </div> 
            
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>

    </div>
    
 
    <script  src="assets/js/api.js"></script>
    <script  src="assets/js/login.js"></script>
    <script  src="assets/js/cart.js" ></script>
    <!-- <script  src="assets/js/detail.js" ></script> -->
    <script  src="assets/js/address.js" ></script>
    <script  src="assets/js/payment.js" ></script>

    <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
    <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
<?php $class = ""; ?>
<?php require_once('twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="" id="product-detail">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page" role="main">
                <div class="page-title">
                    <a href="">
                        <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""></span>
                        back to shop
                    </a>
                </div>

                <div class="product-main">
                    <div class="product-detail-cont">
                        <!-- <div class="browse-box">
                            <img id='cover-pic' src="">
                        </div> -->

                        <!-- <div class="browse-items">
                            <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/white-left-arrow.svg" alt=""></span>
                            <div class="more-images" style="display: flex;"> -->
                        <!-- <div class="browse-ss">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/member-1.jpg">
                                </div> -->
                        <!-- </div>
                            <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/white-right-arrow.svg" alt=""></span>
                        </div> -->

                        <!--Product slider start -->
                        <div class="product-detail-slider-wrapper">
                            <div class="container ">
                                <div class="carousel-container position-relative row">

                                    <!-- Sorry! Lightbox doesn't work - yet. -->

                                    <div id="productCarousel" class="carousel slide w-100" data-ride="carousel"
                                        data-carousel=1>
                                        <div class="carousel-inner inner-carousel">

                                        </div>
                                    </div>

                                    <!-- Carousel Navigation -->

                                    <div id="carousel-thumbs" class="carousel slide " data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <div class="row mx-0">
                                                </div>
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev d-none" href="#carousel-thumbs" role="button"
                                            data-slide="prev">
                                            <span class="fa fa-chevron-left" aria-hidden="true"> </span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next d-none" href="#carousel-thumbs" role="button"
                                            data-slide="next">
                                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- Product slider end -->

                    <!-- </div> -->

                    <div class="product-specification">
                    <h2 class='title'></h2>
                    <h3 ><b class='price'></b></h3>
                    <span id="error"></span>
                    <!-- <div class="spec-row">
                        <label>Size:</label>
                        <div class="lblSize">
                            
                        </div>
                    </div> -->
                    <p id="demo"></p>
                    <div class="size-row">
                        <label>Size:</label>
                        <div class="size-list">
                        </div>
                    </div>

                    <div class="color-row">
                        <label>Color:</label>
                        <div class="color-list">
                            
                        </div>
                    </div>

                    




                    <div class="container prod-desc">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Description
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt="">
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p class="description">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="prod-desc">
                        <label>Description</label>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra tortor aenean mauris sed. Mattis pellentesque eget lectus cras. Sit adipiscing morbi fermentum placerat tellus pulvinar. </p>
                    </div> -->

                    <div class="container prod-spec">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        Specification
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt="">
                                    </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <ul class="specification">
                                            <li>Lorem ipsum</li>
                                            <li>Consectetur adipiscing elit</li>
                                            <li>Sit adipiscing morbi</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- <div class="prod-spec">
                        <label>Specification</label>
                        <ul>
                            <li>Lorem ipsum</li>
                            <li>Consectetur adipiscing elit</li>
                            <li>Sit adipiscing morbi</li>
                        </ul>
                    </div> -->

                    <div class="container prod-spec">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title rating-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        Review & Rating
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt="">
                                    </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in rating-panel">
                                    <!-- <div class="panel-body">
                                        <div class="review-author">
                                            <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/verified.svg"></span> Jane D.Smith 
                                        </div>
                                        <div class="date">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star-rate.svg"> 
                                            <div class="rating">
                                                <label>
                                                  <input type="radio" name="stars" value="1" />
                                                  <span class="icon">★</span>
                                                </label>
                                                <label>
                                                  <input type="radio" name="stars" value="2" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                </label>
                                                <label>
                                                  <input type="radio" name="stars" value="3" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                </label>
                                                <label>
                                                  <input type="radio" name="stars" value="4" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                </label>
                                                <label>
                                                  <input type="radio" name="stars" value="5" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                </label>
                                            </div>
                                            April 2019
                                        </div>
                                        <p>I was looking for a bright light for the kitchen but wanted some item more
                                            modern than a strip light. this one is perfect, very bright and looks great.
                                            I can't comment on interlation as I had an electrition instal it.
                                        </p>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="prod-link">
                        <!--<a href="#">View all 8 review</a>-->
                    </div>

                    <div>
                        
                    </div>

                </div>
                <!-- /page content -->
            </div>
                <div class="common-button">
                    <button class="purple-btn add-to-cart-btn" onclick="addToCart();">Add to Cart</button>
                </div>
        </div>
    </div>
        </div>

   

<script  src="assets/js/api.js"></script>
<script  src="assets/js/login.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/detail.js" ></script>

    <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
    <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

    <?php
    // Program to display URL of current page.
    
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
    else
        $link = "http";
    
    // Here append the common URL characters.
    $link .= "://";
    
    // Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];
    
    // Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];
        
    // Print the link
    // echo $link;
    ?>

    <iframe src="<?php echo $link; ?>" frameborder="0" id="iframeView"></iframe>


<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="login" id="change-password">

        <div class="back-link" style="padding-top:64px;">
            <a href="#"> <img src="./images/icons/chevron-left.svg" alt=""> Reset Password</a>
        </div>

        <div class="container">
            <div class="login-inner">
        
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right">
                
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>
                
                        <p>Please enter a new password and confirm it to reset.</p>
                        <h5 id="passwordcheck" style="display: none;"></h5>

                                <div class="input-field">
                                    <label>New Password</label>
                                    <input id="new_password" type="password" placeholder="Enter New Password" onkeyup="checkPasswordLength()">
                                    <span id="new_password_check"></span>
                                </div>

                                <div class="input-field">
                                    <label>Confirm Password</label>
                                    <input id="confirm_password"type="password" placeholder="Confirm Password" onkeyup="checkPasswordMatch()">
                                    <span id="confirm_password_check"></span>
                                </div>
                                
                        <button id="resetPasswordCheck" onclick="resetPasswordCheck()">Reset Password</button>
                    </div>
                </div> 
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>
    </div>



<script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/login.js" ></script>
<script  src="assets/js/detail.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="" id="buyer-orders">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        

            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page order-detail" role="main">
                <div class="page-title">
                    <a href="javascript:history.go(-1);"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="">Review Product</a>
                </div>
                <div class="dashboard-inner">
                    <div class="product-section">
                        <div class="product-box">

                        <div class="alert alert-success review-alert" role="alert" style="display: none; position: fixed; z-index:999" ></div>
                        <div class="alert alert-danger review-alert" role="alert"  style="display: none; position: fixed; z-index:999"></div>
                            <div class="product-box-inner review-product">
                                <div class="product-box-content">
                                    <div class="product-details">
                                        <img src="images/product-img/product-img.png" alt="image" class="review-product-image">
                                        <div class="box-content">
                                            <p class="review-product-title"></p>

                                            <div class="p-rating">
                                                <div class="rating">
                                                    <label>
                                                      <input type="radio" name="stars" value="1" />
                                                      <span class="icon">★</span>
                                                    </label>
                                                    <label>
                                                      <input type="radio" name="stars" value="2" />
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                    </label>
                                                    <label>
                                                      <input type="radio" name="stars" value="3" />
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                    </label>
                                                    <label>
                                                      <input type="radio" name="stars" value="4" />
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                    </label>
                                                    <label>
                                                      <input type="radio" name="stars" value="5" />
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                      <span class="icon">★</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="input-field">
                                <textarea id="review" placeholder="Write your review"></textarea>
                            </div>

                        </div>

                        <div class="support-btn">
                            <button class="purple-btn" onclick="submitReview()">Submit Feedback</button>
                        </div>
                    </div>
                </div>

            </div>
    </div>

    <script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/login.js" ></script>
<script  src="assets/js/detail.js" ></script>
<script  src="assets/js/review.js" ></script>

<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

<?php $class = ""; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>


    <div class="login" id="buyer-login">

        <div class="back-link">
            <a href="#">Cart <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Information <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Shipping <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""></a>
            <a href="#">Payment</a>
        </div>

        <div class="container" id="buyer-shipping">

            <div class="order-summary">
                <ul>
                    <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/cart-outline.svg" alt=""></li>
                    <li class="text">Order Summary</li>
                    <!-- <li><img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt=""></li> -->
                </ul>
                <h3>KSH <span id="total"></span></h3>
            </div>


            <div class="login-inner">

                <div class="login-left">
                    <!-- <img src=".<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->

                </div>

                <div class="login-right">

                    <div class="login-section">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                        <div class="login-header">
                            Shipping Information
                            <div class="select-shipping-address" style="background: white;">
                                    <div class="product-section">
                                        <div class="product-box row shipping-address">


                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="new-shipping-address">
                            <p>Enter your shipping information.</p>
                            <div class="alert alert-success" role="alert" style="display: none; position: fixed"></div>
                            <div class="alert alert-danger" role="alert"  style="display: none; position: fixed"></div>

                            <div class="input-field">
                                <label>First Name</label>
                                <input type="text" name="first_name" id="first_name" placeholder="First Name" required>
                            </div>

                            <div class="input-field">
                                <label>Last Name</label>
                                <input type="text" name="last_name" id="last_name" placeholder="Last Name" required>
                            </div>

                            <div class="input-field">
                                <label>Apartment</label>
                                <input type="text" placeholder="Apartment" name="appartment" id="apartment" required>
                            </div>

                            <div class="input-field">
                                <label>Locality</label>
                                <input type="text" placeholder="Locality" name="locality" id="locality" required>
                            </div>

                            <div class="input-field">
                                <label>Country/Region</label>
                                <select name="country" class="countries form-control" id="country" onchange="state1()" required>
                                    <option value="">Select Country</option>
                                </select>
                            </div>
                                    

                            <div class="input-field">
                                <label>State</label>
                                <select name="state" class="states form-control" id="state" onchange="city1()" required>
                                    <option value="">Select State</option>
                                </select>
                            </div>

                            <div class="input-field">
                                <label>City</label>
                                <select name="city" class="cities form-control" id="city" required>
                                    <option value="">Select City</option>
                                </select>
                            </div>
                                    

                            <!-- <div class="container">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <label>State</label>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseState">
                                                    Select State
                                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/arrow-down_black.svg" alt="">
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseState" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                        Default checkbox
                                                        </label></li>
                                                    <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                        Default checkbox
                                                        </label></li>
                                                    <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                        Default checkbox
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="input-field">
                                <label>Post Code</label>
                                <input type="number" placeholder="Enter Code" id="postal_code" required>
                            </div>

                        <div class="input-field">
                            <label>Phone Number</label>
                            <div class="phone-no-wrapper">
                            <select id="phone-codes" class="form-control">
                                <option value="254">KE +254</option>
                            </select>
                            <input type="number" placeholder="Enter Phone Number" id="phone_number" required>
                        
                            </div>
                            </div>


                        </div>
                        <button onclick="addAddress()">Continue</button>

                    </div>
                </div>

            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>

    </div>
    <script  src="assets/js/api.js"></script>
<script  src="assets/js/login.js"></script>
<script  src="assets/js/cart.js" ></script>
<!-- <script  src="assets/js/detail.js" ></script> -->
<script  src="assets/js/address.js" ></script>
<script  src="assets/js/payment.js" ></script>

    <?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
    <?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

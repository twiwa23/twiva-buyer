<?php $class = "eshop"; ?>
<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>
        <!--Main Section Start-->
        <div class="" id="buyer-shop">
            <div class="dashboard_container">
                <!--Left Column-->
                <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
                <!--Right Column-->
                <!-- Page Content -->
                <div class="right_col add-product-page">
                    <div class="dashboard-inner">
                        <!--Product Banner-->
                        <div class="product-banner jumbotron">
                            <img class="banner-image" src="<?php echo IMAGES_URI_PATH; ?>/product-img/Rectangle_221.png" alt="Banner"  />
                            <div class="banner-section">
                                <img id="shop-logo" src="<?php echo IMAGES_URI_PATH; ?>/icons/Ellipse 123.svg" alt="" />
                                <div class="banner-content">
                                    <h3 id="shop-name"></h3>
                                    <p class="shop-description"></p>
                                </div>
                            </div>
                        </div>

                        <!--Product Selection-->
                        <div class="product-selection">
                            <!-- <div class="title">
                            <h4>Select Products</h4>
                            <p>Select products for your eShop</p>
                        </div> -->

                            <div class="product-form">
                                <div class="product-filter">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/search.svg" alt="" /></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Search here..." />
                                    </div>

                                    <!--Filter Button-->
                                    <button class="filter-button" type="button">
                                        <span>Filter</span>
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Property 1=Sliders, Property 2=16px.svg" alt="" />
                                    </button>

                                    <div class="filter-wrapper">
                                        <div class="filter-header-wrapper">
                                            <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="" /></span>
                                            <h2 class="filter-title">Filter</h2>
                                        </div>
                                        <div class="dropdown" id="price-dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownPrice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Price Range
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownPrice">
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Price Range 1</label>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="dropdown" id="categories-dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownCategories" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Categories: All Categories
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownCategories">
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                                <li class="form-check dropdown-item">
                                                    <input type="checkbox" class="form-check-input" value="" />
                                                    <label class="form-check-label">Categories 1</label>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="common-button">
                                            <button class="white-bttn">Reset</button>
                                            <button class="purple-btn">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Product Section-->
                        <div class="product-section">
                            <div class="product-box row" id="product-list">
                               
                                

                                <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                    <div class="product-box-content">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg" />
                                        <div class="box-content">
                                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                            <h2>$22.99</h2>
                                            <button class="white-bttn">Select</button>
                                        </div>
                                        <div class="product-rating">
                                            <span class="rating-title">5.0</span>
                                            <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/images/icons/star.svg" alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Page Selection-->
                    <div class="page-selection">
                        <ul class="pagination" id="pagination">
                            
                        </ul>
                    </div>
                </div>
                <!-- /page content -->
            </div>
        </div>
        <script  src="assets/js/api.js"></script>
<script  src="assets/js/shop.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>

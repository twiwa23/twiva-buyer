<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>

<div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        </div>
    </div>
<div class="login" id="buyer-signup">

        <div class="back-link">
            <a href="#"><img src="./images/icons/chevron-left.svg" alt="">Signup</a>
        </div>
        
        <div class="container">


            <div class="login-inner">
            
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right">
                <div class="buyer-titel-wrapper">
                    <h4>Sign Up</h4>
                </div>
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>
                        <div class="input-field">
                            <label>Full Name</label>
                            <input type="text" placeholder="Full name" id="name">
                            <span id="name_error"></span>
                        </div>
                        <div class="input-field">
                            <label>E-mail Address</label>
                            <input type="text" placeholder="email" id="email">
                        </div>
                        <span id="usercheck"></span>
                    
                        <div class="input-field">
                            <label>Password</label>
                            <input type="password" placeholder="*******" id="password">
                            <span id="password_error"></span>
                        </div>
                       <button onclick="signup()">Sign Up</button>

                        <div class="no-account">
                            Already have an account? <a href="/buyer-login.php">Login</a>
                        </div>
                    </div>
                </div> 
            
            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>

    </div>

<script  src="assets/js/login.js" ></script>
<script  src="assets/js/api.js" ></script>
<script  src="assets/js/cart.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
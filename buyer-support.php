<?php require_once('./twiva-config.php'); ?>
<?php include BUYER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include BUYER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

    <div class="login support-heading" id="buyer-login">

        <div class="back-link">
            <a href="#">Need Help?</a>
        </div>
        <div class="alert alert-success" role="alert" style="display: none; position: fixed"></div>
        <div class="alert alert-danger" role="alert"  style="display: none; position: fixed"></div>
        <div class="container" id="buyer-shipping">


            <div class="login-inner">
            
                <div class="login-left">
                    <!-- <img src="../images/banner/login.png"> -->
                
                </div>

                <div class="login-right" id="buyer-support">
                
                    <div class="login-section">
                        <div class="logo"><img src="./images/logo/logo.svg"></div>

                        <div class="container">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <label>Reason</label>
                                            <!-- <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseRegion">
                                                Select Reason
                                                <img src="./images/icons/arrow-down_black.svg" alt="">
                                            </a> -->
                                            <select name="reason" id="reason_id" class="select-option">
                                                <option>Select Reason</option>
                                            </select>
                                        </h4>
                                    </div>
                                    <!-- <div id="collapseRegion" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <ul class="reason-list">
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        
                        <div class="input-field">
                            <label>Describe your problem</label>
                            <textarea id="description"></textarea>
                        </div>
                        
                    </div>
                    <div class="support-btn">
                        <button class="purple-btn" onclick="addSupport()">Contact Support</button>
                    </div>
                </div> 
            
            </div>

            
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="#">Terms & Conditions</a>
        </div>

    </div>

<script  src="assets/js/api.js"></script>
<script  src="assets/js/cart.js" ></script>
<script  src="assets/js/support.js" ></script>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include BUYER_DIRECTORY."/footer/footer-dashboard.php"; ?>
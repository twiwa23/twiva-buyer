        </div>
        <?php include INFLUENCER_DIRECTORY."/auth/all_jquery.php"; ?>
        <script src="<?php echo SCRIPT_URI_PATH; ?>/custom.js"></script>
        <script>
            $(document).ready(function(){
                function setUserInfo(){
                    let user = JSON.parse(localStorage.getItem('_userInfo'));
                    let token = localStorage.getItem('_userToken');
                    if(user || token){
                        window.location.href =  `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php`;
                    }
                }
                setUserInfo();
            })
            
        </script>
    </body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" crossorigin="anonymous"></script>
<script src="<?php echo SCRIPT_URI_PATH; ?>/custom.js"></script>

<script  src="assets/js/sidemenu.js" ></script>
<script  src="assets/js/common.js" ></script>
<script>
    
    setTimeout(() =>{
        let total1 = localStorage.getItem("total");
        total1 = JSON.parse(total1)
        console.log(total1);
        if(total1) {
            $(".order-summary").show();
            $("#total").html(total1);
        }
        if(!total1) {
            $(".order-summary").hide();
            $(".back-link").hide();
        }
    }, 4000);

</script>
<!-- <script src="<?php echo SCRIPT_URI_PATH; ?>/pagination.js"></script> -->

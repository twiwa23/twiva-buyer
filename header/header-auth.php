<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' >
        <title>Sign Up</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous" />
        <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
       
        <!-- Custom styling plus plugins -->
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/animate.min.css" rel="stylesheet">
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom.css" rel="stylesheet">
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/style.css" rel="stylesheet">
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/mobile-view.css" rel="stylesheet" />
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom-style.css" rel="stylesheet" />
    </head>

    <body class="p-0">
        <div class="login pt-5">
            <!-- <div class="back-link">
                <a href="#"> <i class="fas fa-chevron-left"></i> Sign Up</a>
            </div> -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' >
    <title>Shop</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">


    <!-- Custom styling plus plugins -->
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom.css" rel="stylesheet">
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/style.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo IMAGES_URI_PATH; ?>/icons/fav.png" type="image/x-icon">

    <!--Mobile View Styling-->
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/mobile-view.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<body class="nav-md <?php if(isset($class)){ echo $class; } ?>">
    <!--Main Header Start-->
    <header class="header-wrap">
        <!--Main Header Logo-->
        <div class="logo">
            <div class="nav toggle">
                <a id="menu_toggle"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 5.svg" alt="Toggle_Menu"></a>
            </div>
            <a href="https://twiva.co.ke/"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo-white.svg"></a>
        </div>
        <!--Main Header Top Nav Menu-->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <!--Nav Link-->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/img.jpg" alt="">  Devon Lane
                                <!-- <span class=" fa fa-angle-down"></span> -->
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li>
                                    <a href="javascript:;">Profile</a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">Help</a>
                                </li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/bell.svg">
                                <span class="badge bg-yellow">6</span>
                            </a>
                        </li>

                        <li>
                            <a href="/buyer-cart.php" onclick="window.history.forward();">
                                <div id="cart-count" style="color: white;"><sup>0</sup></div><span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/shopping-cart.svg" alt="Shopping-Cart"></span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
    </header>
    <!--Main Header End-->

    <!--Mobile Backdrop-->
    <div class="backdrop"></div>

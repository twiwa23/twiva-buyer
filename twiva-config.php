<?php

// $app_folder = 'twiva-buyer';
$app_folder = '';
// $server = $_SERVER['HTTP_HOST'] .'/twiva-buyer' ;
$server = $_SERVER['HTTP_HOST'];



$scheme = !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on' ? 'http' : 'https';
$base = $scheme.'://'.$server.'/';

$api_base = 'https://api.twiva.co.ke/api/v1';
// $api_base = 'http://127.0.0.1:8000/api/v1';

$image_base = 'https://api.twiva.co.ke/storage/images/products/';

define('HELPERS_URI_PATH', $base.'/helpers');
define('BUYER_URI_PATH', $base.'');
define('BUYER_DASHBOARD_URI_PATH', $base.'/dashboard');
define('BUYER_AUTH_URI_PATH', $base.'/auth');
define('BUSINESS_AUTH_URI_PATH', $base.'/business');
define('SCRIPT_URI_PATH', $base.'/js');
define('STYLESHEET_URI_PATH', $base.'/css');
define('IMAGES_URI_PATH', $base.'/images');

define('BUYER_DIRECTORY', realpath(__DIR__));
define('SCRIPT_DIRECTORY', __DIR__.'/js');
define('STYLESHEET_DIRECTORY',__DIR__.'/css');
define('IMAGES_DIRECTORY', __DIR__.'/images');
define('HELPERS_DIRECTORY', __DIR__.'/helpers');
define('API_URI_PATH', $api_base);
define('base_path', $base);
